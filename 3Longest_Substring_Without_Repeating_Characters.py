class Solution(object):
    def lengthOfLongestSubstring(self, s):
        """
        :type s: str
        :rtype: int
        """
        ans = 0
        last = {}
        start = 0
        for i, x in enumerate(s):
            #x is a repeating character, i is the end of current substring
            if x in last and last[x] >= start:
                ans = max(i - start, ans)
                start = last[x] + 1

            last[x] = i
        return max(ans, len(s) - start)


if __name__ == '__main__':
    test = Solution()
    print test.lengthOfLongestSubstring("abcabcbb")