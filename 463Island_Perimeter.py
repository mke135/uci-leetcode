class Solution(object):
    def islandPerimeter(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        n = len(grid)
        if n > 0 :
            m = len(grid[0])
        else:
            return 0
        result = 0

        def isInvalid(i,j):
            if i >= 0 and j >= 0 and i < n and j < m and grid[i][j] == 1:
                self.invalid += 1

        def validEdges(i, j):
            self.invalid = 0
            map(isInvalid, [i-1, i+1, i, i], [j, j, j-1, j+1])
            return 4 - self.invalid

        for i in xrange(n):
            for j in xrange(m):
                if grid[i][j] == 1:
                    result += validEdges(i, j)
        return result

if __name__ == '__main__':
    test = Solution()
    print test.islandPerimeter([[0,1,0,0],
 [1,1,1,0],
 [0,1,0,0],
 [1,1,0,0]])

