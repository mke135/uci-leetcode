class Solution(object):
    def singleNumber(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        #get the xor result of the nums in the first pass
        #then we know the two distinct number's xor result
        #the elements in nums can be seperate into two groups according to the two distinct number's xor result
        diff = nums[0]
        for i in xrange(1, len(nums)):
            diff ^= nums[i]
        diff &= -diff # get the rightmost set bit
        num1, num2 = 0, 0
        for x in nums:
            if x & diff:
                num1 ^= x
            else:
                num2 ^= x
        return [num1, num2]

if __name__ == '__main__':
    test = Solution()
    print test.singleNumber([1,2,1,3,2,5])