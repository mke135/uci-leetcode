from collections import defaultdict

class Solution(object):
    def judgeCircle(self, moves):
        """
        :type moves: str
        :rtype: bool
        """
        count = defaultdict(int)
        for m in moves:
            count[m] += 1
        if count["R"] == count["L"] and count["U"] == count["D"]:
            return True
        else:
            return False