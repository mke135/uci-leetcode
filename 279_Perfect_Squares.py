class Solution(object):
    def numSquares(self, n):
        """
        :type n: int
        :rtype: int
        """
        dp = [0]
        for x in xrange(1, n + 1):
            start = 1
            min_num = x
            while start * start <= x:
                squ_num = x / (start * start)
                new_num = squ_num + dp[x - start * start * squ_num]
                min_num = min(new_num, min_num)
                start += 1
            dp.append(min_num)
        return dp[n]