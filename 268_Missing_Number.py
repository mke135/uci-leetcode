class Solution(object):
    def missingNumber(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        n = len(nums)
        for x in nums:
            if x >= 0:
                if x < n:
                    nums[x] = (nums[x] + 1) * -1
            if x < 0:
                index = x * -1 - 1
                if index < n:
                    nums[index] =  (nums[index] + 1) * -1
        for i, x in enumerate(nums):
            if x >= 0:
                return i
        return n