class Solution(object):
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        carry = 0
        ret = ListNode(-1) #fake head
        cur = ret
        while l1 or l2:
            x = l1.val if l1 else 0
            y = l2.val if l2 else 0
            new_node = ListNode((x + y + carry) % 10)
            cur.next = new_node
            cur = new_node
            carry = (x + y + carry) // 10
            if l1:
                l1 = l1.next
            if l2:
                l2 = l2.next
        if carry:
            new_node = ListNode(carry)
            cur.next = new_node
        return ret.next