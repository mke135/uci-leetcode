class Solution(object):
    def singleNumber(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        ret = nums[0]
        for i in xrange(1, len(nums)):
            ret ^= nums[i]
        return ret

if __name__ == '__main__':
    test = Solution()
    print test.singleNumber([4,1,2,1,2])

