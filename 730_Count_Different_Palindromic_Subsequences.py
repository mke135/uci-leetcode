class Solution(object):
    def countPalindromicSubsequences(self, S):
        #dp: save number of uniq palindromic subseqs in substring S[i:j]
        #compute dp[i][j]
        #S[i][i] = 1
        #S[i] != S[j]: dp[i + 1][j] + dp[i][j - 1] - dp[i + 1][j - 1]
        #S[i] == S[j]: suppose s[i] = 'a', 
        # case1 like 'a ... a', no character 'a' between leftmost a and rightmost a
        # case2 like 'a ... a ... a', one character 'a' between leftmost and rightmost a
        # case3 like 'a ... a ... a ... a', multiple characters 'a' between leftmost and rightmost a
        n = len(S)
        dp = [[0] * n for _ in xrange(n)]
        #for each character and possible substring leading by it, increase length of substr and save them
        for i in xrange(n):
            dp[i][i] = 1
        for dis in xrange(1, n):
            for i in xrange(n - dis):
                j = i + dis
                if S[i] != S[j]:
                    dp[i][j] = dp[i + 1][j] + dp[i][j - 1] - dp[i + 1][j - 1]
                else:
                    low = i + 1
                    high = j - 1
                    while low <= high and S[low] != S[i]:
                        low += 1
                    while low <= high and S[high] != S[i]:
                        high -= 1
                    if low > high: #case 1
                        dp[i][j] = dp[i + 1][j - 1] * 2 + 2
                    elif low == high: #case 2
                        dp[i][j] = dp[i + 1][j - 1] * 2 + 1
                    else:
                        dp[i][j] = dp[i + 1][j - 1] * 2 - dp[low + 1][high - 1]
                dp[i][j] %= 10^9 + 7
        return dp[0][n - 1]