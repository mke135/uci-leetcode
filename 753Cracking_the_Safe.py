class Solution(object):
    def crackSafe(self, n, k):
        """
        :type n: int
        :type k: int
        :rtype: str
        """
        self.ret = None
        def dfs(s, visited):
            if len(visited) == k ** n:
                self.ret = s
                return True
            prev = s[len(s) - n + 1:]
            for i in xrange(k):
                new_sub = prev + str(i)
                if new_sub not in visited:
                    visited.add(new_sub)
                    if dfs(s + str(i), visited):
                        return True
                    else:
                        visited.remove(new_sub)
            return False
        dfs("0" * n, set(["0" * n]))
        return self.ret

if __name__ == '__main__':
    test = Solution()
    print test.crackSafe(1,2)