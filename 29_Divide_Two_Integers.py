class Solution(object):
    def divide(self, dividend, divisor):
        """
        :type dividend: int
        :type divisor: int
        :rtype: int
        """
        #bit manipulations
        #while dividend >= divisor, we shift the divisor to left by i bit to find the max multification <= divident
        #substract the max multification from the dividend and repeat the process
        #when dividend < divisor, the divide process ends
        #cases of overflow: divisor is 0 or divisor = -1 and dividend = int_min
        if divisor == 0 or (dividend == - 2 ** 31 and divisor == -1):
            return 2 ** 31 - 1
        ret = 0
        sign = -1 if (dividend > 0) ^ (divisor > 0) else 1
        dividend = abs(dividend)
        divisor = abs(divisor)
        while dividend >= divisor:
            tmp = divisor
            mul = 1
            while dividend >= (tmp << 1):
                tmp = tmp << 1
                mul = mul << 1
            ret += mul
            dividend -= tmp
        return ret * sign
            