class Solution(object):
    def candy(self, ratings):
        """
        :type ratings: List[int]
        :rtype: int
        """
        left2right = []#only compare with the left neighbor
        right2left = []
        ret = []
        n = len(ratings)
        for i in xrange(n):
            if i == 0:
                left2right.append(1)
                right2left.append(1)
            else:
                if ratings[i] > ratings[i - 1]:
                    left2right.append(left2right[i - 1] + 1)
                else:
                    left2right.append(1)
                if ratings[n - 1 - i] > ratings[n - i]:
                    right2left.append(right2left[i - 1] + 1)
                else:
                    right2left.append(1)
        for i in xrange(n):
            ret.append(max(left2right[i], right2left[-i-1]))
        return sum(ret)

if __name__ == '__main__':
    test = Solution()
    print test.candy([1,2,3,2])