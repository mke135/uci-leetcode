class Solution(object):
    def convert(self, s, numRows):
        """
        :type s: str
        :type numRows: int
        :rtype: str
        """
        ret = a = [[] for i in xrange(numRows)]
        index = 0
        step = 1
        for x in s:
            ret[index].append(x)
            index += step
            if index >= numRows:
                index = max(numRows - 2, 0)
                step *= -1
            if index < 0:
                index = min(1, numRows - 1)
                step *= -1
        ret_str = ""
        for l in ret:
            ret_str += ''.join(l)
        return ret_str 
        