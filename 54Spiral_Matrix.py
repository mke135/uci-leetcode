class Solution(object):
    def spiralOrder(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: List[int]
        """
        row_start = 0
        row_end = len(matrix) - 1
        if row_end < 0:
            return []
        else:
            col_start = 0
            col_end = len(matrix[0]) - 1
        self.result = []

        def addCol(row, order):
            if order == 1:
                for i in xrange(col_start, col_end + 1, 1):
                    self.result.append(matrix[row][i])
            else:
                for i in xrange(col_end, col_start - 1, -1):
                    self.result.append(matrix[row][i])

        def addRow(col, order):
            if order == 1:
                for i in xrange(row_start, row_end + 1, 1):
                    self.result.append(matrix[i][col])
            else:
                for i in xrange(row_end, row_start - 1, -1):
                    self.result.append(matrix[i][col])

        while row_start <= row_end and col_start <= col_end:
            #add left -> right
            addCol(row_start, 1)
            row_start += 1

            #add top -> bottom
            addRow(col_end, 1)
            col_end -= 1
            
            if row_start <= row_end:
                #add right -> left
                addCol(row_end, -1)
                row_end -= 1

            if col_start <= col_end:
                #add bottom -> top
                addRow(col_start, -1)
                col_start += 1
        return self.result

if __name__ == '__main__':
    test = Solution()
    print test.spiralOrder([[1,2,3,4],[5,6,7,8],[9,10,11,12]])

