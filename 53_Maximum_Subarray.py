class Solution(object):
    def maxSubArray(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        prev = 0
        ret = - 2 ** 31
        for x in nums:
            prev = x if prev < 0 else prev + x
            ret = max(ret, prev)
        return ret