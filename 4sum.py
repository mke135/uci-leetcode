from collections import defaultdict

class Solution(object):
    def fourSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[List[int]]
        """
        sum_hash = defaultdict(list)
        n = len(nums)
        if n < 4:
            return []
        #build the hash of 2sum
        for i in xrange(n - 1):
            for j in xrange(i + 1, n):
                sum_hash[nums[i] + nums[j]].append([i, j])

        def diffIndex(i1, i2):
            return i1[0] != i2[0] and i1[0] != i2[1] and i1[1] != i2[0] and i1[1] != i2[1]
        result = set()
        for x in sum_hash:
            if target - x in sum_hash:
                for p in sum_hash[x]:
                    for q in sum_hash[target - x]:
                        if diffIndex(p, q):
                            ans = sorted([nums[p[0]], nums[p[1]], nums[q[0]], nums[q[1]]])
                            result.add(tuple(ans))
        map(list, result)
        return list(result)

if __name__ == '__main__':
    test = Solution()
    print test.fourSum([1, 0, -1, 0, -2, 2], 0)