# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
from collections import deque
class Solution(object):
    def isSymmetric(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        def cmpRecur(cur, r_cur):
            if (cur and r_cur and cur.val != r_cur.val) or (cur is None and r_cur != None) or\
                     (r_cur is None and cur != None):
                return False
            if cur and r_cur:
                return cmpRecur(cur.left, r_cur.right) and \
                cmpRecur(cur.right, r_cur.left)
            return True

        def cmpIter(root):
            left = deque()
            right = deque()
            if root:
                left.append(root.left)
                right.append(root.right)
            while len(left):
                l = left.popleft()
                r = right.popleft()
                if (l and r and l.val != r.val) or (l is None and r != None) or\
                     (r is None and l != None):
                    return False
                if l and r:
                    left.extend([l.left, l.right])
                    right.extend([r.right, r.left])
            return True

        print cmpIter(root)
        return cmpRecur(root.left, root.right) if root else True

