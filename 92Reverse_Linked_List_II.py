# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    def reverseBetween(self, head, m, n):
        """
        :type head: ListNode
        :type m: int
        :type n: int
        :rtype: ListNode
        """
        fake_header = ListNode(1)
        fake_header.next = head
        i = 0
        cur = fake_header
        left, right, start, end = None, None, None, None #left is the node before m, right is the node after n
        #it is possible that m == n
        while cur:
            to_visit = cur.next
            if i == m - 1:
                left = cur
            if i == m:
                start = cur
            if i > m and i < n:
                cur.next = last
            if i == n:
                cur.next = last
                end = cur
                right = to_visit
                break
            last = cur
            cur = to_visit
            i += 1
        left.next = end
        start.next = right
        return fake_header.next

if __name__ == '__main__':
    test = Solution()
    a1 = ListNode(1)
    a2 = ListNode(2)
    a3 = ListNode(3)
    a4 = ListNode(4)
    a5 = ListNode(5)
    a6 = ListNode(6)
    a1.next = a2
    a2.next = a3
    a3.next = a4
    a4.next = a5
    a5.next = a6
    b = test.reverseBetween(a6, 1, 1)
    cur = b
    while cur:
        print cur.val
        cur = cur.next

