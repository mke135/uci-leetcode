class Solution(object):
    def buildTree(self, preorder, inorder):
        """
        :type preorder: List[int]
        :type inorder: List[int]
        :rtype: TreeNode
        """
        index_inorder = {} 
        for i, x in enumerate(inorder):
            index_inorder[x] = i

        def helper(pre_start, inorder_start, inorder_end):#pre_start index of root val in preorder
            if inorder_start > inorder_end:
                return None
            root = TreeNode(preorder[pre_start])
            pre_start_inorder = index_inorder[preorder[pre_start]]
            root.left = helper(pre_start + 1, inorder_start, pre_start_inorder - 1)
            root.right = helper(pre_start + pre_start_inorder - inorder_start + 1, pre_start_inorder + 1, inorder_end)
            
            return root

        return helper(0, 0, len(inorder) - 1)
            

