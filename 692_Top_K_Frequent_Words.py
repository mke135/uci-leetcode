import heapq as hq
from collections import defaultdict

class Element(object):
    def __init__ (self, word, freq):
        self.word = word
        self.freq = freq
    def __cmp__ (self, other):
        if self.freq == other.freq:
            return self.word > other.word
        return self.freq < other.freq
    def __eq__ (self, other):
        return self.freq == other.freq and self.word == other.word
    def getWord(self):
        return self.word
            
class Solution(object):
    def topKFrequent(self, words, k):
        """
        :type words: List[str]
        :type k: int
        :rtype: List[str]
        """
        count = defaultdict(int)
        for w in words:
            count[w] += 1
        #build heap to get the top k frequent
        heap = []
        for w in count:
            hq.heappush(heap, Element(w, count[w]))
        ret = []
        while len(ret) < k:
            ele= hq.heappop(heap)
            ret.append(ele.getWord())
        return ret

if __name__ == '__main__':
    obj = Solution()
    print obj.topKFrequent(["the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"], 4)