class Solution(object):
    def generateParenthesis(self, n):
        """
        :type n: int
        :rtype: List[str]
        """
        if n == 0:
            return []
        ret = []
        #dfs generate all possible cases
        #when there are more open brackets, can add ")"
        #when the number of open brackers is less than n, can add "("
        def helper(result, depth, open_num):
            if len(result) == 2*n:
                ret.append(result)
            else:
                if depth > 0:
                    helper(result + ")", depth - 1, open_num)
                if open_num < n:
                    helper(result + "(", depth + 1, open_num + 1)

        helper("(", 1, 1)
        return ret