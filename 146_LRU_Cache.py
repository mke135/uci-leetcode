#double linkedlist to store nodes, so we can easily move/remove one node
#hash table save current linked nodes
class LinkedNode(object):
    def __init__(self, key, val):
        self.key = key
        self.val = val
        self.prev = None
        self.next = None

class LRUCache(object):

    def __init__(self, capacity):
        """
        :type capacity: int
        """
        self.head = LinkedNode(-1, -1) #fake head
        self.end = LinkedNode(-1, -1) #fake end
        self.head.next = self.end
        self.end.prev = self.head
        self.dic = {}
        self.size = capacity

    def addNode(self, node):
        node.next = self.head.next
        node.next.prev = node
        self.head.next = node
        node.prev = self.head
    
    def removeNode(self):
        last_node = self.end.prev
        del self.dic[last_node.key]
        last_node.prev.next = self.end
        self.end.prev = last_node.prev

    def get(self, key):
        """
        :type key: int
        :rtype: int
        """
        if key in self.dic:
            ret = self.dic[key].val
            previous = self.dic[key].prev
            post = self.dic[key].next
            previous.next = post
            post.prev = previous

            self.addNode(self.dic[key])
            return ret
        else:
            return -1

    def put(self, key, value):
        """
        :type key: int
        :type value: int
        :rtype: void
        """
        #set or insert node
        if key not in self.dic:
            new_node = LinkedNode(key, value)
            if len(self.dic) == self.size:
                self.removeNode()
            self.dic[key] = new_node
            self.addNode(new_node)
        else:
            self.get(key)
            self.dic[key].val = value


if __name__== '__main__':
    cache = LRUCache(2)
    cache.put(1, 1)
    cache.put(2, 2)
    print cache.get(1)
    cache.put(3, 3)
    print cache.get(2)
    cache.put(4, 4)
    print cache.get(1)
    print cache.get(3)
    print cache.get(4)