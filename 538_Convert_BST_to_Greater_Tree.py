# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def convertBST(self, root):
        """
        :type root: TreeNode
        :rtype: TreeNode
        """
        self.bigger_sum = 0
        def implementGT(cur):
            if cur: 
                right = implementGT(cur.right)
                self.bigger_sum += cur.val
                new_node = TreeNode(self.bigger_sum)
                new_node.right = right
                new_node.left = implementGT(cur.left)
                return new_node
            return None

        return implementGT(root, 0)
            
  