#hash table save key and node
#hash table save freq and the linkednode list
#save current smallest freq
class Linkednode(object):
    def __init__(self, key, val):
        self.key = key
        self.val = val
        self.prev = None
        self.next = None
        self.freq = 1

class LFUCache(object):

    def __init__(self, capacity):
        """
        :type capacity: int
        """
        self.key2node = {}
        self.freq2list = {}#[linkednode head, end, count]
        self.minFreq = 2**31-1 #update when remove/add node
        self.size = capacity
    
    def removeNode(self, freq, node):
        head, end, count = self.freq2list[freq]
        previous = node.prev
        post = node.next
        previous.next = post
        post.prev = previous
        count -= 1
        self.freq2list[freq] = [head, end, count]
        if count == 0 and self.minFreq == freq:
                self.minFreq = 2 ** 31 -1

    def removeMinNode(self, freq):
        head, end, count = self.freq2list[freq]
        del self.key2node[end.prev.key]
        self.removeNode(freq, end.prev)

    def addNode(self, freq, node):
        self.key2node[node.key] = node
        if freq in self.freq2list:
            head, end, count = self.freq2list[freq]
        else:
            head, end, count = Linkednode(-1, -1), Linkednode(-1, -1), 0
            head.next = end
            end.prev = head
        node.next = head.next
        head.next.prev = node
        node.prev = head
        head.next = node
        self.freq2list[freq] = [head, end, count + 1]
        self.minFreq = min(freq, self.minFreq)

    def get(self, key):
        """
        :type key: int
        :rtype: int
        """
        if key in self.key2node:
            cur = self.key2node[key]
            freq = cur.freq
            self.removeNode(freq, cur)
            self.addNode(freq + 1, cur)
            return cur.val
        else:
            return -1

    def put(self, key, value):
        """
        :type key: int
        :type value: int
        :rtype: void
        """
        if key in self.key2node:
            #set value
            cur = self.key2node[key]
            cur.val = value
            freq = cur.freq
            self.removeNode(freq, cur)
            self.addNode(freq + 1, cur)
        else:
            if self.size > 0:
                if len(self.key2node) == self.size:
                    self.removeMinNode(self.minFreq)
                self.addNode(1, Linkednode(key, value))

if __name__ == '__main__':
    test = LFUCache(3)
    test.put(2, 2)
    test.put(1, 1)
    print test.get(2)
    print test.get(1)
    test.put(3, 3)
    test.put(4, 4)
    print test.get(3)