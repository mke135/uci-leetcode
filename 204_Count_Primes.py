class Solution(object):
    def countPrimes(self, n):
        """
        :type n: int
        :rtype: int
        """
        ret = 0
        notPrime = [False] * n
        for i in xrange(2, n): #search in 2~n-1
            if notPrime[i] == False: #prime
                ret += 1
                j = 2
                while i * j < n:
                    notPrime[i*j] = True
                    j += 1
        return ret
    