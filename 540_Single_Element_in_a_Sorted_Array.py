class Solution(object):
    def singleNonDuplicate(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        #binary search
        low, high, n = 0, len(nums) - 1, len(nums)

        def isSingle(cur):
            return (cur == 0 or nums[cur - 1] != nums[cur]) and\
                (cur == n - 1 or nums[cur + 1] != nums[cur])

        def preNoSingle(cur):
            return (cur % 2 and nums[cur - 1] == nums[cur]) or\
                (cur % 2 == 0 and cur < n - 1 and nums[cur + 1] == nums[cur]) 

        while low <= high:
            mid = (low + high)/2

            if isSingle(mid):
                return nums[mid]

            if preNoSingle(mid):
                low = mid + 1
            else:
                high = mid - 1

if __name__ == '__main__':
    test = Solution()
    print test.singleNonDuplicate([5,1,1,2,2])