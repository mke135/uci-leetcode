from collections import defaultdict

class Solution(object):
    def removeDuplicateLetters(self, s):
        """
        :type s: str
        :rtype: str
        """
        #find the leftmost position of each smallest letter in one round
        pos = 0
        count = defaultdict(int)
        for x in s:
            count[x] += 1
        for i, x in enumerate(s):
            pos = i if ord(s[pos]) > ord(s[i]) else pos
            count[x] -= 1
            if count[x] == 0:
                break
        return "" if s == "" else s[pos] + self.removeDuplicateLetters(s[pos:].replace(s[pos], ''))

if __name__ == '__main__':
    test = Solution()
    print(test.removeDuplicateLetters("cbacdcbc"))