class Solution(object):
    def find132pattern(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        #s1 <s3 < s2
        #travel from right to left, update s1, when s3 has value and s1 < s3
        s3 = - 2 ** 31
        stack = []
        for i in xrange(len(nums) - 1, -1, -1):
            if nums[i] < s3:
                return True
            else:
                while len(stack) and nums[i] > stack[-1]:
                    s3 = stack.pop()
            stack.append(nums[i])
        return False


if __name__ == '__main__':
    test = Solution()
    print test.find132pattern([-2,1,1,-2,1,1]) 
