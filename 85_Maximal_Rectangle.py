class Solution(object):
    def maximalRectangle(self, matrix):
        """
        :type matrix: List[List[str]]
        :rtype: int
        """
        #dp, record left, right, and height of each row
        #compute and update area of rectangle in the situation of each row
        m = len(matrix)
        n = len(matrix[0]) if m > 0 else 0
        left, right, height = [0] * n, [n] * n, [0] * n
        ret = 0
        for i in xrange(m):
            #scan each row
            cur_left = 0 #the start of some consecutive '1' in a row
            cur_right = n
            for j in xrange(n):
                #update left, right, height
                if matrix[i][j] == '1':
                    left[j] = max(left[j], cur_left)
                    height[j] += 1
                else:
                    left[j] = 0
                    cur_left = j + 1
                    height[j] = 0

            for j in xrange(n - 1, -1, -1):
                if i == 4:
                    print '&', matrix[i][j], matrix[i][j] == '1', cur_right
                if matrix[i][j] == '1':
                    right[j] = min(right[j], cur_right)
                else:
                    right[j] = n
                    cur_right = j
            for j in xrange(n):
                ret = max(ret, height[j] * (right[j] - left[j]))
        return ret

if __name__ == '__main__':
    test = Solution()
    print test.maximalRectangle([["0","0","0","1","1"],["1","1","1","1","1"]])