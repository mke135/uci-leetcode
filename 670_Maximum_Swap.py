class Solution(object):
    def maximumSwap(self, num):
        """
        :type num: int
        :rtype: int
        """
        dp = []
        str_num = list(str(num))
        for x in xrange(len(str_num) - 1, 0, -1):
            if len(dp):
                if str_num[x] > str_num[dp[-1]]:
                    dp.append(x)
                else:
                    dp.append(dp[-1])
            else:
                dp.append(x)
        for i in xrange(len(str_num) - 1):
            if str_num[i] < str_num[dp[-1]]:
                tmp = str_num[i]
                str_num[i] = str_num[dp[-1]]
                str_num[dp[-1]] = tmp
                break
            dp.pop()
        return int(''.join(str_num))

