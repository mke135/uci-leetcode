# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    def isBalanced(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        self.ret = True
        def treeHeight(cur):
            #return the heights of left and right of current
            #compare and return
            if cur:
                left = treeHeight(cur.left)
                right = treeHeight(cur.right)
                if abs(left - right) > 1:
                    self.ret = False
                return max(left, right) + 1
            else:
                return 0
        treeHeight(root)
        return self.ret

if __name__ == '__main__':
    test = Solution()
    a1 = TreeNode(3)
    a2 = TreeNode(9)
    a3 = TreeNode(20)
    a4 = TreeNode(15)
    a5 = TreeNode(7)
    a1.left = a2
    a2.right = a3
    a3.left = a4
    a3.right = a5
    d = test.isBalanced(a1)
    print d