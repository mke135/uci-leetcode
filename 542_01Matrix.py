from collections import deque
class Solution(object):
    def updateMatrix(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: List[List[int]]
        """
        m = len(matrix)
        n = len(matrix[0]) if m > 0 else 0
        self.ret = [[10000 for i in xrange(n)] for j in xrange(m)]
        #bfs fill in ret
        self.queue = deque()
        for i in xrange(m):
            for j in xrange(n):
                if matrix[i][j] == 0:
                    self.queue.append([i, j, 0])
                    self.ret[i][j] = 0

        def addNeighbors(i, j):
            if i >= 0 and i < m and j >= 0 and j < n:
                if val + 1 < self.ret[i][j]:
                    self.ret[i][j] = val + 1
                    self.queue.append([i, j, val + 1])

        while len(self.queue):
            i, j, val = self.queue.popleft()
            map(addNeighbors, [i - 1, i + 1, i, i], [j, j, j + 1, j - 1])

        return self.ret

if __name__ == '__main__':
    test = Solution()
    print test.updateMatrix([
    [0, 1, 0]])
            
            
            
