class Solution(object):
    def rob(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        last = [0, 0]
        for x in nums:
            new_last = [x + last[1], max(last)]
            last = new_last
        return max(last)