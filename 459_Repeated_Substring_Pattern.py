class Solution(object):
    def repeatedSubstringPattern(self, s):
        """
        :type s: str
        :rtype: bool
        """
        new_s = s[1:] + s[:len(s) - 1]
        return s in new_s