from collections import deque
# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    def levelOrder(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        #use bfs to visit nodes of the same level
        to_visit = deque([root])
        result = []
        while len(to_visit) > 0:
            n = len(to_visit)
            nodes = []
            while n > 0:
                node = to_visit.popleft()
                if node:
                    nodes.append(node.val)
                    map(to_visit.append, [node.left, node.right])
                n -= 1
            if len(nodes) > 0:
                result.append(nodes)
        return result

if __name__ == '__main__':
    test = Solution()
    a1 = TreeNode(3)
    a2 = TreeNode(9)
    a3 = TreeNode(20)
    a4 = TreeNode(15)
    a5 = TreeNode(7)
    a1.left = a2
    a1.right = a3
    a3.left = a4
    a3.right = a5
    d = test.levelOrder(a1)
    print d
                
