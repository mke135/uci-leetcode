class Solution(object):
    def findWords(self, board, words):
        """
        :type board: List[List[str]]
        :type words: List[str]
        :rtype: List[str]
        """
        trie = {}
        results = set()
        n = len(board)

        def makeTrie():
            for w in words:
                cur = trie
                for x in w:
                    if x not in cur:
                        cur[x] = {}
                    cur = cur[x]
                cur["#"] = {}

        def isValid(i, j, cur):
            return i >= 0 and i < n and j >= 0 and j < len(board[0]) and board[i][j] in cur and visited[i][j] == 0

        def addPath():
            word = ''
            for x in path:
                u, v = x
                word += board[u][v]
            results.add(word)

        def dfs():
            data = stack.pop()
            i, j, index, cur = data

            #backtrace
            if index < len(path):
                while(index < len(path)):
                    u,v = path.pop()
                    visited[u][v] = 0
                cur = trie
                for p in path:
                    cur = cur[board[p[0]][p[1]]]

            #if some word is found
            if '#' in cur:
                addPath()

            if isValid(i, j, cur):
                visited[i][j] = 1
                cur = cur[board[i][j]]
                path.append([i, j])
                map(stack.append,
                    [[i - 1, j, index + 1, cur], [i + 1, j, index + 1, cur],
                     [i, j - 1, index + 1, cur], [i, j + 1, index + 1, cur]])

        makeTrie()

        for i in xrange(n):
            for j in xrange(len(board[0])):
                if board[i][j] in trie:
                    visited = [[0 for x in xrange(len(board[0]))] for y in xrange(n)]
                    path = []
                    cur = trie
                    stack = [[i, j, 0, cur]]
                    while len(stack) > 0:
                        dfs()

        return list(results)


if __name__ == '__main__':
    test = Solution()
    print(test.findWords([['o', 'a', 'a', 'n'], ['e', 't', 'a', 'e'],
                          ['i', 'h', 'k', 'r'], ['i', 'f', 'l', 'v']],
                         ["oath", "pea", "eat", "rain"]))
