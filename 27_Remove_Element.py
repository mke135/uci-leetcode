class Solution(object):
    def removeElement(self, nums, val):
        """
        :type nums: List[int]
        :type val: int
        :rtype: int
        """
        n = len(nums)
        ret, cur, tail = n, 0, n - 1
        while cur <= tail:
            if nums[cur] == val:
                nums[cur] = nums[tail]
                tail -= 1
                ret -= 1
            else:
                cur += 1
        return ret

if __name__ == '__main__':
    test = Solution()
    print test.removeElement([3,2,2,3], 3)

