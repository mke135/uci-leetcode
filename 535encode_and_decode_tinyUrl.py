import string
import random

class Codec:
    #use directionaies to memorize
    def __init__(self):
        self.long2short = {}
        self.short2long = {}

    def encode(self, longUrl):
        """Encodes a URL to a shortened URL.
        
        :type longUrl: str
        :rtype: str
        """
        if longUrl in self.long2short:
            return self.long2short[longUrl]
        else:
            key = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(6))
            while key in self.short2long:
                key = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(6))
            #self.long2short[longUrl] = key
            self.short2long[key] = longUrl
            self.long2short[longUrl] = "http://tinyurl.com/" + key
            return "http://tinyurl.com/" + key

    def decode(self, shortUrl):
        """Decodes a shortened URL to its original URL.
        
        :type shortUrl: str
        :rtype: str
        """
        return self.short2long.get(shortUrl[-6:])


# Your Codec object will be instantiated and called as such:
# codec = Codec()
# codec.decode(codec.encode(url))

if __name__ == '__main__':
    codec = Codec()
    print(codec.decode(codec.encode("https://leetcode.com/problems/design-tinyurl")))