class Solution(object):
    def climbStairs(self, n):
        """
        :type n: int
        :rtype: int
        """
        p0, p1 = 1, 1
        for cur in xrange(2, n + 1):
            cur_num = p0 + p1
            p0 = p1
            p1 = cur_num
        return p1