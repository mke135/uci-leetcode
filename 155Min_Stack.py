class MinStack(object):
    def __init__(self):
        """
        initialize your data structure here.
        """
        self.data_stack = []
        self.min_value_stack = []

    def push(self, x):
        """
        :type x: int
        :rtype: void
        """
        self.data_stack.append(x)
        m = len(self.min_value_stack)
        if m > 0 and self.min_value_stack[-1] < x:
            self.min_value_stack.append(self.min_value_stack[-1])
        else:
            self.min_value_stack.append(x)

    def pop(self):
        """
        :rtype: void
        """
        self.data_stack.pop()
        self.min_value_stack.pop()

    def top(self):
        """
        :rtype: int
        """
        return self.data_stack[-1]

    def getMin(self):
        """
        :rtype: int
        """
        return self.min_value_stack[-1]