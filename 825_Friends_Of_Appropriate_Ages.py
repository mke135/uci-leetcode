from collections import defaultdict
class Solution(object):
    def numFriendRequests(self, ages):
        """
        :type ages: List[int]
        :rtype: int
        """
        #count the number of peole of the same age and save it in a hash table
        #compute different ages in hash table to find if they will send request
        #two people send requests to each other only if they are the same age
        def isFriend(a, b):
            return not((b <= 0.5 * a + 7) or\
                b > a or (b > 100 and a < 100))
        ret = 0
        ages_counter = defaultdict(int)
        for x in ages:
            ages_counter[x] += 1
        ages_people = ages_counter.items()
        for i, [age1, people1] in enumerate(ages_people):
            for age2, people2 in ages_people[i:]:
                if isFriend(age1, age2) or isFriend(age2, age1):
                    if age1 == age2:
                        #substract themselves
                        ret += (people1 - 1) * people1
                    else:
                        ret += people1 * people2
        return ret
