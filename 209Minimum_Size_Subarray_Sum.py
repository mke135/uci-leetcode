class Solution(object):
    def minSubArrayLen(self, s, nums):
        """
        :type s: int
        :type nums: List[int]
        :rtype: int
        """
        sums = []#record accumulative sums
        last, start = 0, -1
        for i, x in enumerate(nums):
            last += x
            sums.append(last)
            if last >= s and start == -1:
                start = i #from this index, the sums of subarray can be bigger than s
        if  start == -1:
            return 0
        n = len(nums)
        self.min_len = n
        next_search = 0
        def updateLen(start_search, end):#search for min length of sub that bigger than s
            len_s = end - start_search + 1
            while sums[end] - sums[start_search] >= s:
                start_search += 1
                len_s -= 1
            self.min_len = min(self.min_len, len_s)
            return start_search

        for i in xrange(start, n):
            next_search = updateLen(next_search, i)
        return self.min_len
if __name__ == '__main__':
    test = Solution()
    print test.minSubArrayLen(7, [2,3,1,2,4,3])
