class Solution(object):
    def isAdditiveNumber(self, num):
        """
        :type num: str
        :rtype: bool
        """
        n = len(num)
        def isValid(x, y, end):
            if end == n:
                return True
            z = str(int(x) + int(y))
            return num[end:].startswith(z) and isValid(y, z, end + len(z))

        for i in xrange(1, n/2 + 1):
            if num[0] == '0' and i > 1:
                break
            x = num[:i]
            j = i + 1
            while n - j >= max(i, j - i):
                if num[i] == '0' and j > i + 1:
                    break
                y = num[i : j]
                if isValid(x, y, j) == True:
                    return True
                j += 1
        return False

if __name__ == '__main__':
    test = Solution()
    print test.isAdditiveNumber("19910019")
