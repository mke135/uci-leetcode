# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    def generateTrees(self, n):
        """
        :type n: int
        :rtype: List[TreeNode]
        """
        def getTreeList(start, end):
            tree_list = []
            if start > end:
                return [None]
            if start == end:
                return [TreeNode(start)]
            for i in xrange(start, end + 1):
                left = getTreeList(start, i - 1)
                right = getTreeList(i + 1, end)
                for l in left:
                    for r in right:
                        root = TreeNode(i)
                        root.left = l
                        root.right = r
                        tree_list.append(root)
            return tree_list
        return getTreeList(1, n) if n > 0 else []#generate unique trees from 1 to n

