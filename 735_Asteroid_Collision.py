#coding=utf-8
class Solution(object):
    def asteroidCollision(self, asteroids):
        """
        :type asteroids: List[int]
        :rtype: List[int]
        """
        pos = []#stack of positive direction asteriods
        ret = []
        for x in asteroids:
            if x > 0:
                pos.append(x)
            else:
                if len(pos) == 0:
                    ret.append(x)
                else:
                    max_p = 0
                    while len(pos):
                        p = pos.pop()
                        max_p = max(max_p, p)
                        if p > -x:
                            pos.append(p)
                            break
                        elif p == -x:
                            break
                    #the negative direction asteriod is the biggest
                    if max_p < -x:
                        ret.append(x)
        if len(pos):
            ret.extend(pos)
        return ret

if __name__ == '__main__':
    test = Solution()
    print test.asteroidCollision([-2, 1, -1, 5, -5])