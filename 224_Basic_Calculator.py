#coding=utf-8
class Solution(object):
    def calculate(self, s):
        """
        :type s: str
        :rtype: int
        """
        stack = []
        res = 0
        sign, num = 1, 0
        for x in s:
            if x >= '0' and x <= '9':#x is digit
                num = num * 10 + int(x)
            elif x == '+':
                res += sign * num
                sign, num = 1, 0
            elif x == '-':
                res += sign * num
                sign, num = -1, 0
            elif x == '(':
                stack.append([res, sign])
                res, num, sign = 0, 0, 1
            elif x == ')':
                res += num * sign
                num = 0
                pres, psign = stack.pop()
                res = pres + psign * res
                
        if num:
            res += num * sign
        return res

if __name__ == '__main__':
    test = Solution()
    print test.calculate("2-4-(8+2-6+(8+4-(1)+8-10))")