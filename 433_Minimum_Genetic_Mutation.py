from collections import defaultdict
from collections import deque
class Solution(object):
    def minMutation(self, start, end, bank):
        """
        :type start: str
        :type end: str
        :type bank: List[str]
        :rtype: int
        """
        #construct ajacent list graph of string and the strings it can transform to
        #bfs to find the shortest path that start can transformed to end
        bank_set = set(bank)
        graph = defaultdict(list)

        def addToGraph(string):
            for i in xrange(len(string)):
                for x in ["A", "C", "G", "T"]:
                    new_string = string[:i] + x + string[i + 1:] if i < len(string) - 1 else string[:i] + x
                    if new_string in bank_set:
                        graph[string].append(new_string)

        addToGraph(start)
        hasEnd = 0
        for string in bank:
            addToGraph(string)
            if string == end:
                hasEnd = 1
        if hasEnd == 0:
            return -1
        #bfs to search for end
        queue = deque([start])
        visited = set()
        ret = 0
        while len(queue):
            len_level = len(queue)
            for i in xrange(len_level):
                cur = queue.popleft()
                if cur == end:
                    return ret
                visited.add(cur)
                for x in graph[cur]:
                    if x not in visited:
                        queue.append(x)
            ret += 1
        return -1