from collections import defaultdict

class Solution(object):
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        pairs = defaultdict(list)
        for i, x in enumerate(nums):
            pairs[x].append(i)

        for x in nums:
            #2 same value
            if target - x == x and len(pairs[x]) > 1:
                return pairs[x][:2]
            elif target - x != x and target - x in pairs:
                return [pairs[x][0], pairs[target-x][0]]


if __name__ == '__main__':
    test = Solution()
    print test.twoSum([3,2,4], 6)
