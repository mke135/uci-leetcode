# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    def rob(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        def implement_rob(cur):
            if cur is None:
                return [0, 0]
            #first ele:if not include current node, the max of sum, 
            #second ele: if included current node, the max of sum
            left = implement_rob(cur.left)
            right = implement_rob(cur.right)
            ret = []
            ret.append(max(left) + max(right))
            ret.append(cur.val + left[0] + right[0])
        return max(implement_rob(root))
