class Solution(object):
    def numTrees(self, n):
        """
        :type n: int
        :rtype: int
        """
        num = [0] * (n + 1)
        num[0], num[1] = 1, 1
        for i in xrange(2, n + 1):
            for j in xrange(1, i + 1):
                num[i] += num[j - 1] * num[i - j]
        return num[-1]
