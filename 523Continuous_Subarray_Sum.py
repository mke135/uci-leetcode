class Solution(object):
    def checkSubarraySum(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: bool
        """
        dic = {} #key is the previous sum % k, val is the index
        #if the visited_sum % k previous appearred, then the subarray of sum of nk must occured
        visited_sum = 0
        for i, x in enumerate(nums):
            visited_sum += x
            if k:
                visited_sum %= k
            if visited_sum == 0 and i > 0:
                return True
            if visited_sum in dic:
                if i - dic[visited_sum] > 1 or visited_sum == 0:
                    return True
            else:
                dic[visited_sum] = i

        return False


if __name__ == '__main__':
    test = Solution()
    print(test.checkSubarraySum([1,1], 2))
