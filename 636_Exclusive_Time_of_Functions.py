class Solution(object):
    def exclusiveTime(self, n, logs):
        """
        :type n: int
        :type logs: List[str]
        :rtype: List[int]
        """
        funcs = []#save function id, start time and time used by internal functions
        ret = [0] * n
        for l in logs:
            str_id, status, time = l.split(":")
            if status == "start":
                funcs.append([int(str_id), int(time), 0])
            else:
                time = int(time)
                f_id, start, children_duration = funcs.pop()
                ret[f_id] += time - start - children_duration + 1
                if len(funcs):
                    funcs[-1][2] += time - start + 1
        return ret

