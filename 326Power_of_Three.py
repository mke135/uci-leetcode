class Solution(object):
    def isPowerOfThree(self, n):
        """
        :type n: int
        :rtype: bool
        """
        #3**20 > 2**31 - 1 and 3**19 < 2**31 - 1
        return n > 0 and 3 ** 19 % n == 0

if __name__ == '__main__':
    test = Solution()
    print test.isPowerOfThree(81)