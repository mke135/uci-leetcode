class Solution(object):
    def nextPermutation(self, nums):
        """
        :type nums: List[int]
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        #index is the position that [index:] is descending order, nums[index]>nums[index - 1]
        n = len(nums)
        index = n - 1
        while index > 0:
            if nums[index] > nums[index - 1]:
               break
            index -= 1

        def swap(nums, i, j):
            tmp = nums[i]
            nums[i] = nums[j]
            nums[j] = tmp

        def reverse(nums, start, end):
            while end > start:
                swap(nums, start, end)
                start += 1
                end -= 1

        if index == 0:
            reverse(nums, 0, n - 1)
        else:
            #pick the smallest val > nums[index - 1] to make a smaller increasement
            exchange_index, j = index, n - 1
            while j > index - 1:
                if nums[j] > nums[index - 1]:
                    exchange_index = j
                    break
                j -= 1
            swap(nums, exchange_index, index - 1)
            reverse(nums, index, n - 1)
        print nums
        
if __name__ == '__main__':
    test = Solution()
    test.nextPermutation([2,3,1])