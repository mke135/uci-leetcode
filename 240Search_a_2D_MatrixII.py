class Solution(object):
    def searchMatrix(self, matrix, target):
        """
        :type matrix: List[List[int]]
        :type target: int
        :rtype: bool
        """
        row_start = 0
        row_end = len(matrix) - 1
        if row_end >= 0:
            col_start = 0
            col_end = len(matrix[0]) - 1
        else:
            return False
        cur = [row_start, col_end]
        while row_start <= row_end and col_start <= col_end:
            i, j = cur
            if matrix[i][j] == target:
                return True
            if matrix[i][j] < target:
                row_start += 1
            elif matrix[i][j] > target:
                col_end -= 1
            cur = [row_start, col_end]
        return False

if __name__ == '__main__':
    test = Solution()
    print test.searchMatrix([
  [1,   4,  7, 11, 15]
], -1)