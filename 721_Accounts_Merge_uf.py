from collections import defaultdict

class UF(object):
    def __init__ (self, n):
        self.parents = range(n)
    
    def find(self, x):
        if x == self.parents[x]:
            return x
        self.parents[x] = self.find(self.parents[x])
        return self.parents[x]

    def union(self, x, y):
        self.parents[self.find(x)] = self.find(y)

class Solution(object):
    def accountsMerge(self, accounts):
        """
        :type accounts: List[List[str]]
        :rtype: List[List[str]]
        """
        email2name = {}
        id2email = {}
        email2id = {}
        for account in accounts:
            name = account[0]
            for i in xrange(1, len(account)):
                email2name[account[i]] = name
                if account[i] not in email2id:
                    id2email[len(id2email)] = account[i]
                    email2id[account[i]] = len(email2id)
        #uf
        n = len(email2id)
        uf = UF(n)
        for account in accounts:
            for e1 in account[1:]:
                for e2 in account[1:]:
                    uf.union(email2id[e1], email2id[e2])
        id2group = defaultdict(list)
        for i in xrange(n):
            id2group[uf.find(i)].append(id2email[i])
        ret = []
        for i, group in id2group.iteritems():
            account = [email2name[group[0]]]
            account.extend(sorted(group))
            ret.append(account)
        return ret