class Solution:
    def nextGreaterElement(self, n):
        """
        :type n: int
        :rtype: int
        """
        nums = list(str(n))
        for i in xrange(len(nums) - 2, -1, -1):
            if nums[i] < nums[i + 1]:
                for j in xrange(len(nums) - 1, i, -1):
                    if nums[j] > nums[i]:
                        tmp = nums[i]
                        nums[i] = nums[j]
                        nums[j] = tmp
                        ret = nums[:i+1] + sorted(nums[i+1:])
                        int_ret = int(''.join(ret))
                        if len(ret) > 10 or int_ret > 2 ** 31:
                            return -1
                        else:
                            return int(''.join(ret))
        return -1