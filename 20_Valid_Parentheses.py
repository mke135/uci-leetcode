class Solution(object):
    def isValid(self, s):
        """
        :type s: str
        :rtype: bool
        """
        stack = []
        close2open = {"}":"{", "]": "[", ")": "("}
        for x in s:
            if x in ["{", "[", "("]:
                stack.append(x)
            elif len(stack) == 0:
                return False
            else:
                open_bracket = stack.pop()
                if close2open[x] != open_bracket:
                    return False
        return False if len(stack) else True