class Solution(object):
    def containsDuplicate(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        visited = set()
        for x in nums:
            if x in visited:
                return True
            visited.add(x)
        return False