#coding=utf-8
class Solution(object):
    def countArrangement(self, N):
        """
        :type N: int
        :rtype: int
        """
        self.ret = 0
        def findArran(cur, visited):
            if cur == 1:
                self.ret += 1
            else:
                for x in xrange(1, N + 1):
                    if x not in visited and (cur % x == 0 or x % cur == 0):
                        visited.add(x)
                        print x, cur, visited
                        findArran(cur - 1, visited)
                        visited.remove(x)
        findArran(N, set())
        return self.ret

if __name__ == '__main__':
    test = Solution()
    print test.countArrangement(4)