class Solution(object):
    def isBipartite(self, graph):
        """
        :type graph: List[List[int]]
        :rtype: bool
        """
        #bipartitie: vertices can be divided into two disjoint and independent sets
        #the vertices of edge must in different set
        #use -1, 0, 1 to label vertices, the unvisited vertex is labeled 0
        #for each node, label it and label neighbors the opposite 
        #when to label a node, if the visiting node already has the opposite label, return false
        n = len(graph)
        self.labels = [0] * n

        def labelNode(cur, label):
            if self.labels[cur]:
                 return self.labels[cur] == label
            else:
                self.labels[cur] = label
                for x in graph[cur]:
                    if not labelNode(x, -label):
                        return False
                return True

        for i in xrange(n):
            if self.labels[i] == 0 and not labelNode(i, 1):
                return False
        return True

if __name__ == '__main__':
    test = Solution()
    print(test.isBipartite([[1,2,3],[0,2],[0,1,3],[0,2]]))