from collections import defaultdict

class Solution(object):
    def reconstructQueue(self, people):
        """
        :type people: List[List[int]]
        :rtype: List[List[int]]
        """
        heights = set()#record different heights of people
        h_dic = defaultdict(list)
        result = []
        for x in people:
            heights.add(x[0])
            h_dic[x[0]].append(x)
        heights = list(heights)
        heights.sort(reverse = True)
        for h in heights:
            p = h_dic[h]
            sorted_p = sorted(p, key=lambda x:x[1])
            for x in sorted_p:
                result.insert(x[1], x)
        return result

if __name__ == '__main__':
    test = Solution()
    print(test.reconstructQueue([[7,0], [4,4], [7,1], [5,0], [6,1], [5,2]]))
                