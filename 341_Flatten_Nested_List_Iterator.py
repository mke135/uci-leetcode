# """
# This is the interface that allows for creating nested lists.
# You should not implement it, or speculate about its implementation
# """
#class NestedInteger(object):
#    def isInteger(self):
#        """
#        @return True if this NestedInteger holds a single integer, rather than a nested list.
#        :rtype bool
#        """
#
#    def getInteger(self):
#        """
#        @return the single integer that this NestedInteger holds, if it holds a single integer
#        Return None if this NestedInteger holds a nested list
#        :rtype int
#        """
#
#    def getList(self):
#        """
#        @return the nested list that this NestedInteger holds, if it holds a nested list
#        Return None if this NestedInteger holds a single integer
#        :rtype List[NestedInteger]
#        """

class NestedIterator(object):

    def __init__(self, nestedList):
        """
        Initialize your data structure here.
        :type nestedList: List[NestedInteger]
        """
        self.path = [[0, nestedList]]#record the index and the list of nestedIntegers
        self.nextInt = None 

    def generateNext(self):
        while len(self.path) > 0 and self.nextInt is None:
            c, cl = self.path.pop() 
            while len(self.path) > 0 and c >= len(cl):
                c, cl = self.path.pop()
            if c < len(cl):
                if cl[c].isInteger():
                    self.nextInt = cl[c].getInteger()
                    self.path.append([c + 1, cl])
                else:
                    self.path.append([c + 1, cl])
                    self.path.append([0, cl[c].getList()])                   

    def next(self):
        """
        :rtype: int
        """
        if self.nextInt is None:
            self.generateNext()
        ret = self.nextInt
        self.nextInt = None
        return ret

    def hasNext(self):
        """
        :rtype: bool
        """
        if self.nextInt is None:
            self.generateNext()
        return False if self.nextInt is None else True

# Your NestedIterator object will be instantiated and called as such:
# i, v = NestedIterator(nestedList), []
# while i.hasNext(): v.append(i.next())