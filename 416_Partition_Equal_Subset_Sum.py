class Solution(object):
    def canPartition(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        sum_nums = sum(nums)
        if sum_nums % 2:
            return False
        target = sum_nums / 2
        n = len(nums)
        dp = [[False for i in xrange(target + 1)] for j in xrange(n + 1)]
        dp[0][0] = True
        for i in xrange(1, n + 1):
            for j in xrange(target + 1):
                has_i = dp[i - 1][j - nums[i - 1]] if j - nums[i - 1] >= 0 else False
                drop_i = dp[i - 1][j]
                dp[i][j] = has_i or drop_i
        return dp[n][target]

if __name__ == '__main__':
    test = Solution()
    print test.canPartition([1, 2, 3, 5])
