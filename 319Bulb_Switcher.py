import numpy
class Solution(object):
    def bulbSwitch(self, n):
        """
        :type n: int
        :rtype: int
        """
        return int(numpy.sqrt(n))

if __name__ == '__main__':
    test = Solution()
    print test.bulbSwitch(4)
