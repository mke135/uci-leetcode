#coding=utf-8
class Solution(object):
    def countBinarySubstrings(self, s):
        """
        :type s: str
        :rtype: int
        """
        prev, cur = 0, 1
        ret = 0
        for i in xrange(1, len(s)):
            if s[i] != s[i - 1]:
                prev = cur
                cur = 1
            else:
                cur += 1
                
            if cur <= prev:
                ret += 1
                
        return ret
