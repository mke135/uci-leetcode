from collections import deque
from collections import defaultdict

class Solution(object):
    def ladderLength(self, beginWord, endWord, wordList):
        """
        :type beginWord: str
        :type endWord: str
        :type wordList: List[str]
        :rtype: int
        """
        #construct a adjacent list, transformed words are the neighbors of certain word.
        graph = defaultdict(set)
        n = len(wordList)
        word_set = set(wordList)

        def canTransform(w1, w2):
            diff = 0
            for i in xrange(len(w1)):
                if w1[i] != w2[i]:
                    diff += 1
                    if diff > 1:
                        return False
            return True

        def addToGraph(word, start):
            for i in xrange(start, n):
                if canTransform(word, wordList[i]):
                    graph[word].add(wordList[i])
                    graph[wordList[i]].add(word)

        def addToGraphByChangeChars(word,
                                    start):  # O(len(word) * 26) for one word
            for i in xrange(len(word)):
                for c in list('abcdefghijklmnopqrstuvwxyz'):
                    new_word = word[:i] + c + word[(i + 1):]
                    if new_word in word_set:
                        graph[word].add(new_word)

        if n > 26 * len(beginWord):
            addToGraph = addToGraphByChangeChars

        #construct adjacent list graph
        addToGraph(beginWord, 0)
        for i in xrange(n):
            addToGraph(wordList[i], i + 1)

        queue = deque()
        queue.append([[beginWord], beginWord])
        result = []
        def bfs():
            len_level = len(queue)  # from last level
            for i in xrange(len_level):
                path, cur_word = queue.popleft()
                if len(path) == n:
                    return
                if endWord in graph[cur_word]:
                    result.append(path + [endWord])

                for w in graph[cur_word]:
                    queue.append([path + [w], w])

            if len(queue) > 0 and len(result) == 0:
                bfs()
        bfs()
        return result


if __name__ == '__main__':
    test = Solution()
    print test.ladderLength( "red", "tax", ["ted", "tex", "red", "tax", "tad", "den", "rex", "pee"])
