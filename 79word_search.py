#coding=utf-8
class Solution:
    def exist(self, board, word):
        """
        :type board: List[List[str]]
        :type word: str
        :rtype: bool
        """
        #搜索到首字符以后在它的上下左右蔓延寻找在word里面的letter，已经访问的标记visited
        n = len(board)
        m = len(board[0])

        def dfsMatch():
            #有回退的情况下把visited的重新改为未访问
            while index < len(path):
                p, q = path.pop()
                visited[p][q] = 0

            if index < len(word) and x >= 0 and x < n and y >= 0 and y < m and board[x][y] == word[index] and visited[x][y] == 0:
                visited[x][y] = 1
                path.append((x,y))
                map(stack.append,
                    ((x + 1, y, index + 1), (x - 1, y, index + 1),
                     (x, y + 1, index + 1), (x, y - 1, index + 1)))

        for i in xrange(n):
            for j in xrange(m):
                if board[i][j] == word[0]:
                    visited = [[0 for u in xrange(m)] for v in xrange(n)]
                    stack = [(i,j,0)]
                    path = []
                    index = 0
                    while len(stack) > 0 and index < len(word):
                        x, y, index = stack.pop()
                        dfsMatch()

                    if index == len(word):
                        return True

        return False


if __name__ == '__main__':
    test = Solution()
    print(test.exist(
        [["A","B","C","E"],["S","F","E","S"],["A","D","E","E"]],
        "ABCESEEEFS"))
