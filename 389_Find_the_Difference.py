from collections import defaultdict

class Solution(object):
    def findTheDifference(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: str
        """
        count = defaultdict(int)
        for x in s:
            count[x] += 1
        for y in t:
            if y not in count or count[y] == 0:
                return y
            else:
                count[y] -= 1