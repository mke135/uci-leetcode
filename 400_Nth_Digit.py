class Solution(object):
    def findNthDigit(self, n):
        """
        :type n: int
        :rtype: int
        """
        sum_digits = 0
        for i in xrange(0, 10):
            sum_digits += 9 * 10 ** i
            if sum_digits >= n:
                num_digit = i + 1
                dis = n - (sum_digits - 9 * 10 ** i)
                target = 10 ** i + (dis - 1) // num_digit
                return int(str(target)[(dis - 1)%num_digit])

if __name__ == '__main__':
    test = Solution()
    print test.findNthDigit(2)
            
