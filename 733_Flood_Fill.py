class Solution(object):
    def floodFill(self, image, sr, sc, newColor):
        """
        :type image: List[List[int]]
        :type sr: int
        :type sc: int
        :type newColor: int
        :rtype: List[List[int]]
        """
        oldColor = image[sr][sc]
        stack = [[sr, sc]]

        def colorImage(x, y):
            if x >= 0 and x < len(image) and y >= 0 and y < len(image[0]) and image[x][y] == oldColor:
                image[x][y] = newColor
                map(stack.append, [[x - 1, y], [x + 1, y], [x, y - 1], [x, y + 1]])

        if oldColor != newColor:
            while len(stack) > 0:
                x, y = stack.pop()
                colorImage(x, y)
        return image
            
        