# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def constructMaximumBinaryTree(self, nums):
        """
        :type nums: List[int]
        :rtype: TreeNode
        """
        #decreasing values nodes is right children of the first node
        #we save the decreasing values in stack and make them a right child tree
        #when some bigger number(bigger than the smallest val in stack) appear
        #the nodes with smaller value in the stack should be its left chilren
        #save the number into stack and keep it decreasing(pop smaller ones)
        stack = []
        for x in nums:
            new_node = TreeNode(x)
            if len(stack) and x > stack[-1]:
                new_node.left = stack.pop()
            if len(stack):
                stack[-1].right = new_node
            stack.append(new_node)
        return stack[0]