from collections import defaultdict

class Solution(object):
    def findMinHeightTrees(self, n, edges):
        """
        :type n: int
        :type edges: List[List[int]]
        :rtype: List[int]
        """
        #a tree, the roots should be the node last touched when we cut leaves
        edges_dic = defaultdict(set)
        for x,y in edges:
            edges_dic[x].add(y)
            edges_dic[y].add(x)

        def getLeaves():
            leaves = []
            for x in edges_dic:
                if len(edges_dic[x]) == 1:
                    leaves.append(x)
            return leaves

        while len(edges_dic) > 2:
            leaves = getLeaves()
            #cut leaves
            for x in leaves:
                for y in edges_dic[x]:
                    edges_dic[y].remove(x)
                del edges_dic[x]

        result = []
        if n < 2:
            result = range(n)
        for x in edges_dic:
            result.append(x)
        
        return result

if __name__ == '__main__':
    test = Solution()
    print(test.findMinHeightTrees(2, [[1, 0]]))


        

