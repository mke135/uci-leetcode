# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        #two pointers located at the end of l
        stack1, stack2 = [], []
        p = l1
        while p:
            stack1.append(p.val)
            p = p.next
        p = l2
        while p:
            stack2.append(p.val)
            p = p.next
        last = 0
        ret = None
        while len(stack1) or len(stack2):
            num1 = stack1.pop() if len(stack1) else 0
            num2 = stack2.pop() if len(stack2) else 0
            node = ListNode(sum([num1, num2, last]) % 10)
            node.next = ret
            ret = node
            last = sum([num1, num2, last]) // 10
        if last:
            node = ListNode(last)
            node.next = ret
            ret = node
        return ret


if __name__ == '__main__':
    test = Solution()
    a1 = ListNode(7)
    a2 = ListNode(2)
    a3 = ListNode(4)
    a4 = ListNode(3)
    a1.next=a2
    a2.next=a3
    a3.next=a4
    a5 = ListNode(5)
    a6 = ListNode(6)
    a7 = ListNode(4)
    a5.next = a6
    a6.next=a7
    opt = test.addTwoNumbers(a1, a5)
    while opt:
       print opt.val
       opt = opt.next