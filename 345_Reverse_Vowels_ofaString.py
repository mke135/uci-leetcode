class Solution(object):
    def reverseVowels(self, s):
        """
        :type s: str
        :rtype: str
        """
        vowels = set(["a", "e", "i", "o", "u", "A", "E", "I", "O", "U"])
        positions = []
        letters = []
        for i, x in enumerate(s):
            if x in vowels:
                positions.append(i)
                letters.append(x)
        ret = list(s)
        for i in xrange(len(letters)):
            x = letters.pop()
            ret[positions[i]] = x
        return ''.join(ret)

if __name__ == '__main__':
    test = Solution()
    print test.reverseVowels("Aa")