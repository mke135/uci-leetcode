from collections import defaultdict

class Solution(object):
    def groupAnagrams(self, strs):
        """
        :type strs: List[str]
        :rtype: List[List[str]]
        """
        words = defaultdict(list)
        for x in strs:
            words[''.join(sorted(x))].append(x)
        results = []
        for k in words:
            results.append(words[k])
        return results

if __name__ == '__main__':
    test = Solution()
    print test.groupAnagrams(["eat", "tea", "tan", "ate", "nat", "bat"])