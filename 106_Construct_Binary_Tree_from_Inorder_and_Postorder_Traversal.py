class Solution(object):
    def buildTree(self, inorder, postorder):
        """
        :type inorder: List[int]
        :type postorder: List[int]
        :rtype: TreeNode
        """
        index = {}
        for i, x in enumerate(inorder):
            index[x] = i

        def helper(in_start, in_end, post_start, post_end):
            if post_end < post_start:
                return None
            root = TreeNode(postorder[post_end])
            index_root_inorder = index[postorder[post_end]]
            left_in_end = index_root_inorder - 1
            len_left = left_in_end - in_start
            root.left = helper(in_start, left_in_end, post_start, post_start + len_left)
            root.right = helper(index_root_inorder + 1, in_end, post_start + len_left + 1, post_end - 1)
            return root

        return helper(0, len(inorder) - 1, 0, len(postorder) - 1)

