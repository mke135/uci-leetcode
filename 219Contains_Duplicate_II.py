from collections import defaultdict

class Solution(object):
    def containsNearbyDuplicate(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: bool
        """
        letters = defaultdict(int)
        for i, x in enumerate(nums):
            if x in letters and i - letters[x] <= k:
                return True
            letters[x] = i
        return False

if __name__ == '__main__':
    test = Solution()
    print test.containsNearbyDuplicate([1,2,3,1,2,3], 2)