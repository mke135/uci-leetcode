class Solution(object):
    def findMedianSortedArrays(self, a, b):
        """
        :type a: List[int]
        :type b: List[int]
        :rtype: float
        """
        n = len(a)
        m = len(b)
        
        if n < m:
            return self.findMedianSortedArrays(b, a)
        
        higher = a[-1]
        lower = a[0]
        if m > 0:
            higher = max(b[-1], a[-1]) + 1
            lower = min(b[0], a[0]) - 1
        
        l = 0
        r = m * 2
        while l <= r:
            mid_b = (l + r) // 2
            mid_a = n + m - mid_b
            
            la = lower if mid_a == 0 else a[(mid_a - 1) // 2]
            ra = higher if mid_a == n * 2 else a[mid_a // 2]
            lb = lower if mid_b == 0 else b[(mid_b - 1) // 2]
            rb = higher if mid_b == m * 2 else b[mid_b // 2]
            
            if la > rb:
                l = mid_b + 1
            elif lb > ra:
                r = mid_b - 1
            else:
                return (max(la, lb) + min(ra, rb)) / 2.0