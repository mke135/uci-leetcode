class Solution(object):
    def threeSum(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        nums.sort()
        n = len(nums)
        i, j, k = 0, 0, n - 1
        if n < 3 or nums[i] > 0 or nums[k] < 0:
            return []
        result = []
        while i < n and nums[i] <= 0:
            j, k = i + 1, n - 1
            while j < k and nums[k] >= 0:
                if nums[i] + nums[j] + nums[k] > 0:
                    k -= 1
                    while k > j and nums[k] == nums[k + 1]:
                        k -= 1
                elif nums[i] + nums[j] + nums[k] == 0:
                    result.append([nums[i], nums[j], nums[k]])
                    j += 1
                    k -= 1
                    while k > j and nums[k] == nums[k + 1]:
                        k -= 1
                    while j < k and nums[j] == nums[j - 1]:
                        j += 1
                elif nums[i] + nums[j] + nums[k] < 0:
                    j += 1
                    while j < k and nums[j] == nums[j - 1]:
                        j += 1
            i += 1
            while i < n and nums[i] == nums[i - 1]:
                i += 1
        return result

if __name__ == '__main__':
    test = Solution()
    print test.threeSum([0, 0, 0])