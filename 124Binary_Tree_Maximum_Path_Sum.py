# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    def maxPathSum(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        self.last_max = - 2 ** 31
        def postOrder(cur):
            if cur:
                left = postOrder(cur.left)
                right = postOrder(cur.right)
                if left is not None and right is not None:
                    self.last_max = max(cur.val, left + cur.val, right + cur.val, left + right + cur.val, self.last_max)
                    return max(cur.val, left + cur.val, right + cur.val)
                elif left is not None and right is None:
                    self.last_max = max(cur.val, left + cur.val, self.last_max)
                    return max(cur.val, left + cur.val)
                elif right is not None and left is None:
                    self.last_max = max(cur.val, right + cur.val, self.last_max)
                    return max(cur.val, right + cur.val)
                else:
                    self.last_max = max(cur.val, self.last_max)
                    return cur.val
            else:
                return None
        postOrder(root)
        return self.last_max

if __name__ == '__main__':
    test = Solution()
    a1 = TreeNode(6)
    a2 = TreeNode(3)
    a3 = TreeNode(-4)
    a4 = TreeNode(-5)
    a5 = TreeNode(0)
    a6 = TreeNode(2)
    a7 = TreeNode(2)
    a8 = TreeNode(4)
    a1.left = a2
    a1.right = a3
    a2.left = a4
    a2.right = a5
    a4.right = a6
    a6.left = a7
    a7.left = a8
    d = test.maxPathSum(a1)
    print d