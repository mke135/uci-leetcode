from Queue import Queue

class Solution(object):
    def letterCombinations(self, digits):
        """
        :type digits: str
        :rtype: List[str]
        """
        #for each input in digits, we get each element of previous saved combinations of letter and add the incoming letters
        D = [0, 0, "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"]
        res = Queue()
        if len(digits) > 0:
            res.put("")
        for i, x in enumerate(digits):
            y = res.get()
            while len(y) is i:
                for z in D[int(x)]:
                    res.put(y + z)
                y = res.get()
            res.put(y)

        return list(res.queue)


if __name__ == '__main__':
    test = Solution()
    print(test.letterCombinations(""))