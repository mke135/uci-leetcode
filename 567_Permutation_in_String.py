from collections import defaultdict

class Solution(object):
    def checkInclusion(self, s1, s2):
        """
        :type s1: str
        :type s2: str
        :rtype: bool
        """
        target = defaultdict(int)
        for x in s1:
            target[x] += 1
        start, cur = 0, 0
        while cur < len(s2):
            if s2[cur] in target:
                if target[s2[cur]] > 0:
                    target[s2[cur]] -= 1
                    cur += 1
                    if sum(target.values()) == 0:
                        return True
                else:
                    while start <= cur and target[s2[cur]] <= 0:
                        target[s2[start]] += 1
                        start += 1
            else:
                while start < cur:
                    target[s2[start]] += 1
                    start += 1
                cur += 1
                start = cur 
        return False         

if __name__ == '__main__':
    test = Solution()
    print test.checkInclusion("ab", "eidbaooo")
        