from collections import defaultdict
class Trie(object):

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.trie = defaultdict(dict)

    def insert(self, word):
        """
        Inserts a word into the trie.
        :type word: str
        :rtype: void
        """
        cur = self.trie
        for w in word:
            if w not in cur:
                cur[w] = defaultdict(dict)
            cur = cur[w]
        if '#' not in cur:
            cur['#'] = True      

    def search(self, word):
        """
        Returns if the word is in the trie.
        :type word: str
        :rtype: bool
        """
        cur = self.trie
        for w in word:
            if w not in cur:
                return False
            cur = cur[w]
        return '#' in cur

    def startsWith(self, prefix):
        """
        Returns if there is any word in the trie that starts with the given prefix.
        :type prefix: str
        :rtype: bool
        """
        cur = self.trie
        for w in prefix:
            if w not in cur:
                return False
            cur = cur[w]
        return True