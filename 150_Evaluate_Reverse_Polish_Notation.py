#coding=utf-8
class Solution(object):
    def evalRPN(self, tokens):
        """
        :type tokens: List[str]
        :rtype: int
        """
        #把数字存进stack，遇到操作符就出栈两个进行运算而后将结果存回stack
        stack = []
        for x in tokens:
            if x == '+':
                a = stack.pop()
                b = stack.pop()
                stack.append(b + a)
            elif x == '-':
                a = stack.pop()
                b = stack.pop()
                stack.append(b - a)
            elif x == '*':
                a = stack.pop()
                b = stack.pop()
                stack.append(b * a)
            elif x == '/':
                a = stack.pop()
                b = stack.pop()
                if a*b > 0:
                    stack.append(b // a)
                else:
                    tmp = -b / a 
                    stack.append(-tmp)
            else:
                stack.append(int(x))
        return stack[0]

if __name__ == '__main__':
    test = Solution()
    print test.evalRPN(["10","6","9","3","+","-11","*","/","*","17","+","5","+"])