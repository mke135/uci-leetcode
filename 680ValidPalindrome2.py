class Solution(object):
    def validPalindrome(self, s):
        """
        :type s: str
        :rtype: bool
        """
        def match(i, j, diff):
            while i < j:
                if s[i] == s[j]:
                    i += 1
                    j -= 1
                elif diff == 1:
                    return False
                else:
                    #delete 1 char to make a valid palindrome
                    if match(i, j - 1, 1):
                        return True
                    elif match(i + 1, j, 1)
                        return True
                    else:
                        return False
            return True
        return match(0, len(s) - 1, 0)


if __name__ == '__main__':
    test = Solution()
    print(test.validPalindrome("eeccccbebaeeabebccceea"))
