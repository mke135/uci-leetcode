from collections import defaultdict

class Solution(object):
    def subarraySum(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: int
        """
        #sum(nums[i:j]) == k then sum(nums[:j]) - sum(nums[:i]) == k, 
        #sum(nums[:j]) - k should have some i stored in pre_sum
        pre_sum = defaultdict(int)
        pre_sum[0] = 1 #no ele is added then the sum is 0
        cur_sum = 0
        result = 0
        for x in nums:
            cur_sum += x
            if cur_sum - k in pre_sum:
                result += pre_sum[cur_sum - k]
            pre_sum[cur_sum] += 1
        return result

if __name__ == '__main__':
    test = Solution()
    print test.subarraySum([1,1,1], 2)