class Solution(object):
    def maxArea(self, height):
        """
        :type height: List[int]
        :rtype: int
        """
        ret = 0
        l2r, r2l = [[0, 0]], [[0, 0]]

        for i in xrange(len(height)):
            if height[i] > l2r[-1][0]:
                l2r.append([height[i], i + 1])
            if height[-i-1] > r2l[-1][0]:
                r2l.append([height[-i-1], n - i])

        for i in xrange(1, len(l2r)):
            h1, start = l2r[i]
            for j in xrange(len(r2l) - 1, 0, -1):
                h2, end = r2l[j]
                if start > end:
                    break
                ret = max(ret, min(h1, h2) * (end - start))
        return ret