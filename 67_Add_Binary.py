class Solution(object):
    def addBinary(self, a, b):
        """
        :type a: str
        :type b: str
        :rtype: str
        """
        ret = ""
        l1 = len(a)
        l2 = len(b)
        carry = 0
        for i in xrange(1, max(l1, l2) + 1):
            x = int(a[-i]) if i <= l1 else 0
            y = int(b[-i]) if i <= l2 else 0
            ret += str((x + y + carry) % 2)
            carry = (x + y + carry) / 2
        if carry:
            ret += '1'
        return ret[::-1] if len(ret) else 0
if __name__ == '__main__':
    test = Solution()
    print test.addBinary("11", "1")