# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def reverseList(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        last = None
        cur = head
        #iterative method
        while cur:
            next_cur = cur.next
            cur.next = last
            last = cur
            cur = next_cur
        return last
