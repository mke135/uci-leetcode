class Solution(object):
    def isPalindrome(self, s):
        """
        :type s: str
        :rtype: bool
        """
        if s == "":
            return True
        else:
            s_char = re.sub('[^0-9a-zA-Z]+', '', s).lower()
            s_rev = s_char[::-1]
            return True if s_char == s_rev else False