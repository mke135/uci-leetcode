from collections import defaultdict
class Solution(object):
    def minWindow(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: str
        """
        #use two pointers start and end to find valid window
        #use a hash target to save character and number, counter save the number of letters in t
        #set start to 0 and move end to make the window contains all the needed characters in target
        #and then move start to find the shortest
        #then increase start by one and repeat the process until start meets end
        target = defaultdict(int)
        counter = len(t)
        for x in t:
            target[x] += 1
            counter += 1
        n = len(s)
        if counter > n or counter == 0:
            return ""
        start, end = 0, 0
        ret = ""
        while end < n:
            #move end to make count have necessary characters
            while end < n and counter > 0:
                if s[end] in target:
                    if target[s[end]] > 0:
                        counter -= 1
                    target[s[end]] -= 1
                end += 1
            #move start to find the shortest
            while counter == 0:
                if s[start] in target:
                    target[s[start]] += 1
                    if target[s[start]] > 0:
                        candidate = s[start:end]
                        ret = candidate if len(candidate) < len(ret) or ret == "" else ret 
                        counter += 1  
                start += 1

        return ret
if __name__ == '__main__':
    test = Solution()
    print(test.minWindow("cabwefgewcwaefgcf", "cae"))