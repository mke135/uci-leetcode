class Solution(object):
    def calculateMinimumHP(self, dungeon):
        """
        :type dungeon: List[List[int]]
        :rtype: int
        """
        m = len(dungeon)
        n = len(dungeon[0]) if m > 0 else 0
        min_hp = [[(2 ** 31 - 1) for i in xrange(n + 1)] for j in xrange(m + 1)]
        min_hp[m][n - 1], min_hp[m - 1][n] = 1, 1
        for i in xrange(m - 1, -1, -1):
            for j in xrange(n - 1, -1, -1):
                need_hp = min(min_hp[i + 1][j], min_hp[i][j + 1]) - dungeon[i][j]
                #in this position ,to be alive, the min hp we need
                min_hp[i][j] = need_hp if need_hp > 0 else 1
        return min_hp[0][0]
        

if __name__ == '__main__':
    test = Solution()
    print test.calculateMinimumHP([[1,-4,5,-99],[2,-2,-2,-1]])
