# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None
import heapq as hq
class Solution(object):
    def mergeKLists(self, lists):
        """
        :type lists: List[ListNode]
        :rtype: ListNode
        """
        #complexity: nlogk
        heap = []
        for l in lists:
            if l:
                hq.heappush(heap, (l.val, l))
        ret = ListNode(-1) #fake head
        ret_cur = ret
        while len(heap):
            small, cur = hq.heappop(heap)
            new_node = ListNode(small)
            ret_cur.next = new_node
            ret_cur = ret_cur.next
            if cur.next:
                hq.heappush(heap, (cur.next.val, cur.next))
        return ret.next
            
