class Solution:
    def numDecodings(self, s):
        """
        :type s: str
        :rtype: int
        """
        #use dp to calculate the number of decodings
        #only need to record d[i-1], d[i -2]
        n = len(s)
        d = [0]
        d[0] = 1 if int(s[0]) > 0 else 0
        if n > 1:
            d1 = 0
            if int(s[1]) > 0:
                d1 += d[0]
            if int(s[0:2]) >= 10 and int(s[0:2]) < 27:
                d1 += 1
            d.append(d1)

        if n > 2:
            for i in xrange(2, n):
                di = 0
                if int(s[i]) > 0:
                    di += d[-1]
                if int(s[i - 1:i + 1]) >= 10 and int(s[i - 1:i + 1]) < 27:
                    di += d[-2]
                d[-2] = d[-1]
                d[-1] = di
        return d[-1]


if __name__ == '__main__':
    test = Solution()
    print(test.numDecodings("102023"))
