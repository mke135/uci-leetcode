# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def diameterOfBinaryTree(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        self.ret = 0
        def helper(cur):
            #return the max depth of current node
            if cur:
                left = helper(cur.left)
                right = helper(cur.right)
                self.ret = max(self.ret, left + right + 1)
                return max(left, right) + 1
            return 0
        helper(root)
        return max(0, self.ret - 1)