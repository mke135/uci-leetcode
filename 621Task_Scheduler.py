from collections import defaultdict

class Solution(object):
    def leastInterval(self, tasks, n):
        """
        :type tasks: List[str]
        :type n: int
        :rtype: int
        """
        max_occ = 0
        count = defaultdict(int)
        for x in tasks:
            count[x] += 1
            max_occ = max(count[x], max_occ)
        num_max = 0
        for k in count:
            if count[k] == max_occ:
                num_max += 1
        return max(len(tasks), (n + 1) * (max_occ-1) + num_max)


if __name__ == '__main__':
    test = Solution()
    print test.leastInterval(["A", "A", "A", "B", "B", "B"], 2)
