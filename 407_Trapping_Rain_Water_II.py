import heapq as hq
class Solution(object):
    def trapRainWater(self, heightMap):
        """
        :type heightMap: List[List[int]]
        :rtype: int
        """
        #the lowest elevation of border decide the amount of trapped water
        #start from borders of the map, put them into a heap
        #every time pop the smallest one, then we know its neighbors' lowest border
        if len(heightMap) == 0 or len(heightMap[0]) == 0:
            return 0
        heap = []
        ret = 0
        m, n = len(heightMap), len(heightMap[0])
        #input borders into heap
        visited = [[0] * n for _ in xrange(m)]
        for i in xrange(m):
            for j in xrange(n):
                if i in [0, m - 1] or j in [0, n - 1]:
                    visited[i][j] = 1
                    hq.heappush(heap, (heightMap[i][j], i, j))
        #visit all 
        def helper(i, j, h):
            if 0 <= i < m and 0 <= j < n and visited[i][j] == 0:
                visited[i][j] = 1
                ret += max(0, h - heightMap[i][j])
                hq.heappush(heap, (max(heightMap[i][j], h), i, j))

        while len(heap):
            h, i, j = hq.heappop(heap)
            map(helper, [i - 1, i + 1, i, i], [j, j, j - 1, j + 1], [h, h, h, h])

        return ret