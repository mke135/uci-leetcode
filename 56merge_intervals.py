# Definition for an interval.
class Interval(object):
    def __init__(self, s=0, e=0):
        self.start = s
        self.end = e


class Solution(object):
    def merge(self, intervals):
        """
        :type intervals: List[Interval]
        :rtype: List[Interval]
        """
        res = []
        for i, y in enumerate(sorted(intervals, key=lambda x: x.start)):
            if i == 0:
                res.append(y)
            else:
                tmp = res[- 1]
                if y.start <= tmp.end:
                    res[-1].end = max(y.end, tmp.end)
                else:
                    res.append(y)
        return res


if __name__ == '__main__':
    test = Solution()
    a1 = Interval(1,4)
    a2 = Interval(4,6)
    a3 = Interval(8,10)
    a4 = Interval(15,18)
    tmp = test.merge([a1, a2, a3, a4])
    for x in tmp:
        print x.start, x.end
