from collections import defaultdict

class Solution(object):
    def isAnagram(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: bool
        """
        count = defaultdict(int)
        for x in s:
            count[x] += 1

        for y in t:
            if len(count) == 0 or y not in count:
                return False
            else:
                count[y] -= 1
                if count[y] == 0:
                    del count[y]
                    
        return False if len(count) > 0 else True

if __name__ == '__main__':
    test = Solution()
    print test.isAnagram("", "car")
