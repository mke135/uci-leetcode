class Solution(object):
    def trap(self, height):
        """
        :type height: List[int]
        :rtype: int
        """
        right_height,left_height = [0],[0] #record the highest right/left elevation
        result = 0
        #the height can be empty
        len_h = len(height)
        for i in xrange(len_h-2, -1, -1):
            last = height[i + 1]
            right_height.append(max(last, right_height[-1]))
        for i in xrange(1, len_h):
            last = height[i - 1]
            left_height.append(max(last, left_height[-1]))
        for i in xrange(len_h):
            trap_val = min(left_height[i], right_height[- i - 1]) - height[i]
            if trap_val > 0:
                result += trap_val
        return result


if __name__ == '__main__':
    test = Solution()
    print test.trap([])
