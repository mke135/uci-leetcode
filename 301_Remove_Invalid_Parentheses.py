class Solution:
    def removeInvalidParentheses(self, s):
        """
        :type s: str
        :rtype: List[str]
        """
        #left to right remove redundant ")"
        #reverse s, remove redundant "("
        ret = set()
        def helper(string, depth, cur, last, cha, r_cha):
            #left to right
            if cur == len(string):
                if cha == "(":
                    helper(string[::-1], 0, 0, 0, ")", "(")
                else:
                    ret.add(string[::-1])
            else:
                if string[cur] == cha:
                    helper(string, depth + 1, cur + 1, last, cha, r_cha)
                elif string[cur] == r_cha:
                    if depth == 0:
                        for i in xrange(last, cur + 1):
                            if string[i] == r_cha:
                                helper(string[:i] + string[i + 1:], 0, cur, i, cha, r_cha)
                    else:
                        helper(string, depth - 1, cur + 1, last, cha, r_cha)
                else:
                    helper(string, depth, cur + 1, last, cha, r_cha)

        helper(s, 0, 0, 0, "(", ")")
        return list(ret)