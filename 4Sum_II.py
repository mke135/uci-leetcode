from collections import defaultdict

class Solution(object):
    def fourSumCount(self, A, B, C, D):
        """
        :type A: List[int]
        :type B: List[int]
        :type C: List[int]
        :type D: List[int]
        :rtype: int
        """
        sum_ab = defaultdict(int)
        sum_cd = defaultdict(int)
        for x in A:
            for y in B:
                sum_ab[x+y] += 1
        for x in C:
            for y in D:
                sum_cd[x+y] += 1
        ret = 0
        for x in sum_ab:
            if -x in sum_cd:
                ret += sum_ab[x] * sum_cd[-x]
        return ret


if __name__ == '__main__':
    test = Solution()
    print test.fourSumCount([ 1, 2], [-2,-1], [-1, 2], [ 0, 2])