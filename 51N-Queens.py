class Solution(object):
    def solveNQueens(self, n):
        """
        :type n: int
        :rtype: List[List[str]]
        """
        #queens[i] record the y-indice of queen in row i, we can only have 1 queen in 1 row and 1 column
        result = []
        def dfs(queens, q_diffs, q_sums):
            x = len(queens)
            if x == n:
                result.append(queens)
                return
            else:
                #i is the index of column
                for i in xrange(n):
                    if i not in queens and x + i not in q_sums and x - i not in q_diffs:
                        dfs(queens + [i], q_diffs + [x - i], q_sums + [x + i])
        dfs([], [], [])
        return [["." * i + "Q" + "." * (n - i - 1) for i in rec] for rec in result]


if __name__ == '__main__':
    test = Solution()
    print test.solveNQueens(4)