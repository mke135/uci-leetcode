# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    def detectCycle(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        #start: the distance between the start of list and start of circle
        #meet: the distance between the start of circle and the node when walker and runner meet
        #because start + meet = k = nr, k is the steps when the two pointers meet
        #2k - k = nr
        #then start = r - meet
        walker, runner = head, head
        has_circle = False
        while walker and runner and walker.next and runner.next and runner.next.next:
            walker = walker.next
            runner = runner.next.next
            if walker == runner:
                has_circle = True
                break
        if has_circle:
            cur = head
            while cur != walker:
                cur = cur.next
                walker = walker.next
            return cur
        else:
            return None

if __name__ == '__main__':
    test = Solution()
    a1 = ListNode(1)
    a2 = ListNode(2)
    a3 = ListNode(3)
    a1.next = a2
    a2.next = a3
    a3.next = a1
    val = test.detectCycle(a1)
    print val.val