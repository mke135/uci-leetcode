class Solution(object):
    def threeSumClosest(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        nums.sort()
        n = len(nums)
        diff = 2 ** 31 - 1
        result = None
        #n2 search, any more simple solution?
        for i in xrange(n - 2):
            start = i + 1
            end = n - 1
            while start < end:
                sum_ele = sum([nums[i], nums[start], nums[end]])
                if sum_ele > target:
                    end -= 1
                    while end > start and nums[end] == nums[end + 1]:
                        end -= 1
                elif sum_ele < target:
                    start += 1
                    while end > start and nums[start] == nums[start - 1]:
                        start += 1
                else:
                    return target
                if abs(target - sum_ele) < diff:
                    diff = abs(target - sum_ele)
                    result = sum_ele
        return result

if __name__ == '__main__':
    test = Solution()
    print test.threeSumClosest([0,2,1,-3], 0)