import heapq as hq
class Solution(object):
    def findKthLargest(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: int
        """
        heap = []
        for x in nums:
            hq.heappush(heap, x)
            if len(heap) > k:
                hq.heappop(heap)
        return hq.heappop(heap)