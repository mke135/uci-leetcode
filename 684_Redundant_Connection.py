class Solution(object):
    def findRedundantConnection(self, edges):
        """
        :type edges: List[List[int]]
        :rtype: List[int]
        """
        to_visit = set()
        ret = []
        for u, v in edges:
            map(to_visit.add, [u, v])
        for u, v in edges:
            if v not in to_visit and u not in to_visit:
                ret.append([u, v])
            else:
                to_visit.discard(u)
                to_visit.discard(v)
        return ret[-1]

if __name__ == '__main__':
    test = Solution()
    print test.findRedundantConnection([[1,2], [2,3], [3,4], [1,4], [1,5]])
            



