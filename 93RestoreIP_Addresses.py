class Solution(object):
    def restoreIpAddresses(self, s):
        """
        :type s: str
        :rtype: List[str]
        """
        n = len(s)
        if n < 4 or n > 12:
            return []
        #dfs search all possible ip
        self.results = []

        def searchIp(ip, start):
            if start == n:
                if len(ip) == 4:
                    self.results.append('.'.join(ip))
                return
            if len(ip) >= 4:
                return
            #add 1 digit
            searchIp(ip + [s[start]], start + 1)
            #add 2 digits
            if s[start] > '0' and start + 1 < n:
                searchIp(ip + [s[start:start + 2]], start + 2)
            #add 3 digits
            if s[start] in ['1', '2'] and start + 2 < n and int(s[start:start + 3]) <= 255:
                searchIp(ip + [s[start:start + 3]], start + 3)
        
        searchIp([], 0)
        return self.results
if __name__ == '__main__':
    test = Solution()
    print test.restoreIpAddresses("1111")

