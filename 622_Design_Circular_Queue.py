class MyCircularQueue(object):
    def __init__(self, k):
        """
        Initialize your data structure here. Set the size of the queue to be k.
        :type k: int
        """
        self.size = k
        self.cq = [0] * k
        self.start = 0
        self.length = 0

    def enQueue(self, value):
        """
        Insert an element into the circular queue. Return true if the operation is successful.
        :type value: int
        :rtype: bool
        """
        if self.length == self.size:
            return False
        self.cq[(self.start + self.length) % self.size] = value
        self.length += 1
        return True  

    def deQueue(self):
        """
        Delete an element from the circular queue. Return true if the operation is successful.
        :rtype: bool
        """
        if self.length == 0:
            return False
        self.start = (self.start + 1) % self.size
        self.length -= 1
        return True

    def Front(self):
        """
        Get the front item from the queue.
        :rtype: int
        """
        return self.cq[self.start] if self.length else -1

    def Rear(self):
        """
        Get the last item from the queue.
        :rtype: int
        """
        return self.cq[(self.start + self.length - 1) % self.size] if self.length else -1

    def isEmpty(self):
        """
        Checks whether the circular queue is empty or not.
        :rtype: bool
        """
        return self.length == 0

    def isFull(self):
        """
        Checks whether the circular queue is full or not.
        :rtype: bool
        """
        return self.length == self.size