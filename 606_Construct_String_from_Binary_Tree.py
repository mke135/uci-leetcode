class Solution(object):
    def tree2str(self, t):
        """
        :type t: TreeNode
        :rtype: str
        """
        def preTravel(cur):
            ret = ""
            if cur:
                ret += str(cur.val)
                left = preTravel(cur.left)
                right = preTravel(cur.right)
                if len(right):
                    ret += "(" + left + ")" + "(" + right + ")"
                elif len(left):
                    ret += "(" + left + ")"
            return ret

        return preTravel(t)
