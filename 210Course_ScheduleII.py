from collections import defaultdict
from collections import deque
class Solution(object):
    def findOrder(self, numCourses, prerequisites):
        """
        :type numCourses: int
        :type prerequisites: List[List[int]]
        :rtype: List[int]
        """
        #ordered graph and bfs 
        #make th graph
        pre_courses = defaultdict(list)
        indegree = [0] * numCourses
        for x,y in prerequisites:
            pre_courses[y].append(x)
            indegree[x] += 1
        zeros = deque() #courses has no prerequisites
        for i, x in enumerate(indegree):
            if x == 0:
                zeros.append(i)
        ret = []
        for i in xrange(numCourses):#add all courses into ret
            if len(zeros) == 0:
                return []
            cur = zeros.popleft()
            ret.append(cur)
            for c in pre_courses[cur]:
                indegree[c] -= 1
                if indegree[c] == 0:
                    zeros.append(c)
        return ret

if __name__ == '__main__':
    test = Solution()
    print test.findOrder(4, [[1,0],[2,0],[3,1],[3,2]])

        


            


