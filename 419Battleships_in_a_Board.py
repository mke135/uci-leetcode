class Solution(object):
    def countBattleships(self, board):
        """
        :type board: List[List[str]]
        :rtype: int
        """
        n = len(board)
        result = 0

        def isNewBattle():
            return board[i][j] == 'X' and ((i == 0 and j == 0) \
            or (i == 0 and j > 0 and board[i][j - 1] != 'X')\
            or (i > 0 and j == 0 and board[i - 1][j] != 'X') \
            or (i > 0 and j > 0 and board[i][j - 1] != 'X' \
            and board[i - 1][j] != 'X'))

        for i in xrange(n):
            for j in xrange(len(board[0])):
                if isNewBattle():
                    result += 1
        return result

if __name__ == '__main__':
    test = Solution()
    print(test.countBattleships([['X', '.', '.', 'X'],
    ['.', '.', '.', 'X'], ['.', '.', '.', 'X']]))