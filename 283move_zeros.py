class Solution(object):
    def moveZeroes(self, nums):
        """
        :type nums: List[int]
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        index = 0
        for x in nums:
            if x is not 0:
                nums[index] = x
                index += 1

        if index < len(nums):
            for i in xrange(index,len(nums)):
                nums[i] = 0
        print nums


if __name__ == '__main__':
    test = Solution()
    test.moveZeroes([0, 2, 1, 0, 3, 7, 0, 0])
