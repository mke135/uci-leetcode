class Solution:
    def findDisappearedNumbers(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        ret = []
        for x in nums:
            abs_x = abs(x)
            if nums[abs_x - 1] > 0:
                nums[abs_x - 1] *= -1
        for i, x in enumerate(nums):
            if x > 0:
                ret.append(i + 1)
        return ret