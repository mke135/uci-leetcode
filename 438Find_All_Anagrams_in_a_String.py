class Solution(object):
    def findAnagrams(self, s, p):
        """
        :type s: str
        :type p: str
        :rtype: List[int]
        """
        count = [0] * 26 #count occurrences of letter
        for x in p:
            count[ord(x) - ord('a')] += 1
        self.target = count + []
        result = []
        start = 0

        def goBack(start, i, x):
            while self.target[ord(x) - ord('a')] != 0 and start <= i:
                self.target[ord(s[start]) - ord('a')] += 1
                start += 1
            return start

        for i, x in enumerate(s):
            index = ord(x) - ord('a')
            if count[index] == 0:
                start = i + 1
                self.target = count + []
                continue

            self.target[index] -= 1
            if self.target[index] < 0:
                start = goBack(start, i, x)
            if self.target[index] == 0 and sum(self.target) == 0:
                result.append(start)
        
        return result

if __name__ == '__main__':
    test = Solution()
    print(test.findAnagrams("abab", "ab"))
                
                
