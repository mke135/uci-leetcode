class Solution(object):
    def countAndSay(self, n):
        """
        :type n: int
        :rtype: str
        """
        #keep save the number of consecutive numbers and the number
        cur = ["1"]
        ret = ["1"]
        for i in xrange(1, n):
            count = 0
            ret = []
            for j, x in enumerate(cur): 
                if j == 0 or cur[j - 1] == x:
                    count += 1
                else:
                    ret.extend([str(count), str(cur[j - 1])])
                    count = 1
            ret.extend([str(count), str(cur[-1])])
            cur = ret
        return ''.join(ret)


