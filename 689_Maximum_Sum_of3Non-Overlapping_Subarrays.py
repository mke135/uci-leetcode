class Solution(object):
    def maxSumOfThreeSubarrays(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: List[int]
        """
        #compute accumutation sum to simplify following computation
        #compute and save maximum k subarray for nums from left to right and from right to left
        #k_sum is the middle k-subarray, move from k to n-2k
        sums_nums = []
        last = 0
        for x in nums:
            last += x
            sums_nums.append(last)
        n = len(nums)
        left2right = []
        for i in xrange(n - 3*k + 1):
            if i == 0:
                left2right.append([sums_nums[k - 1], 0])
            else:
                new_sum = sums_nums[i + k - 1] - sums_nums[i - 1]
                if new_sum > left2right[-1][0]:
                    left2right.append([new_sum, i])
                else:
                    left2right.append(left2right[-1])
        right2left = []
        for i in xrange(n - k, 2*k - 1, -1):
            if i == n - k:
                right2left.append([sums_nums[n - 1] - sums_nums[i - 1], i])
            else:
                new_sum = sums_nums[i + k - 1] - sums_nums[i - 1]
                if new_sum >= right2left[-1][0]:
                    right2left.append([new_sum, i])
                else:
                    right2left.append(right2left[-1])
        ret_max = - 2 ** 31
        ret = []
        index = 0
        for i in xrange(k, n - 2*k + 1):
            k_sum = sums_nums[i + k - 1] - sums_nums[i - 1]
            all_sum = k_sum + left2right[index][0] + right2left[-index-1][0]
            if all_sum > ret_max:
                ret_max = all_sum
                ret = [left2right[index][1], i, right2left[-index-1][1]]
            index += 1
        return ret
