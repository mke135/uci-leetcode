class Solution(object):
    def titleToNumber(self, s):
        """
        :type s: str
        :rtype: int
        """
        ret = 0
        for i in xrange(len(s)):
            ret += 26 ** i * (ord(s[-i - 1]) - ord('A') + 1)
        return ret

if __name__ == '__main__':
    test = Solution()
    print test.titleToNumber("ZY")