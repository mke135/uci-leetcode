#coding=utf-8
class Solution(object):
    def findDiagonalOrder(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: List[int]
        """
        pace = [[-1, 1], [1, -1]]
        pace_type = 0
        ret = []
        m = len(matrix)
        n = len(matrix[0]) if m > 0 else 0
        cur = [0, 0]
        while len(ret) < m * n:
            ret.append(matrix[cur[0]][cur[1]])
            cur[0] += pace[pace_type][0]
            cur[1] += pace[pace_type][1]
            if cur[0] >= m:
                cur[0] -= 1
                cur[1] += 2
                pace_type = (pace_type + 1)%2
            if cur[1] >= n:
                cur[0] += 2
                cur[1] -= 1
                pace_type = (pace_type + 1)%2
            if cur[0] < 0:
                cur[0] = 0
                pace_type = (pace_type + 1)%2
            if cur[1] < 0:
                cur[1] = 0
                pace_type = (pace_type + 1)%2
        return ret

