class Solution(object):
    def PredictTheWinner(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        n = len(nums)
        cache = [[None for i in xrange(n)] for j in xrange(n)]
        def helper(start, end, cache):
            if cache[start][end] is None:
                cache[start][end] = nums[start] if start == end \
                else max((nums[start] - helper(start + 1, end, cache)), (nums[end] - helper(start, end - 1, cache)))
            return cache[start][end]

        return helper(0, n - 1, cache) >= 0


if __name__ == '__main__':
    test = Solution()
    print test.PredictTheWinner([1, 5, 233, 7])