class Solution(object):
    def lowestCommonAncestor(self, root, p, q):
        """
        :type root: TreeNode
        :type p: TreeNode
        :type q: TreeNode
        :rtype: TreeNode
        """
        cur = root
        max_val = max(p.val, q.val)
        min_val = min(p.val, q.val)
        while cur:
            if cur.val <= max_val and cur.val >= min_val:
                return cur
            if cur.val < min_val:
                cur = cur.right
            elif cur.val > max_val:
                cur = cur.left
