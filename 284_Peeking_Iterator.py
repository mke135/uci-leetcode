class PeekingIterator(object):
    def __init__(self, iterator):
        """
        Initialize your data structure here.
        :type iterator: Iterator
        """
        self.front = None
        self.iter = iterator

    def peek(self):
        """
        Returns the next element in the iteration without advancing the iterator.
        :rtype: int
        """
        if self.front is None:
            self.front = self.iter.next()
        return self.front
            

    def next(self):
        """
        :rtype: int
        """
        ret = None
        if not self.front is None:
            ret = self.front
            self.front = None
        else:
            ret = self.iter.next()
        return ret

    def hasNext(self):
        """
        :rtype: bool
        """
        if not self.front is None or self.iter.hasNext():
            return True
        return False