class Solution:
    # @param n, an integer
    # @return an integer
    def reverseBits(self, n):
        #exchange first 16 bits and second 16 bits
        n = (n >> 16) | (n << 16)
        #exchange 8 bits sections in 16 bits
        n = ((n & 0xff00ff00) >> 8) | ((n & 0x00ff00ff) << 8)
        #exchange 4 bits sctions in 8 bits
        n = ((n & 0xf0f0f0f0) >> 8) | ((n & 0x0f0f0f0f) << 8)
        #exchange 2 bits sections in 4 bits
        n = ((n & 0xcccccccc) >> 8) | ((n & 0x33333333) << 8)
        #exchange 1 bits sections in 2 bits
        n = ((n & 0xaaaaaaaa) >> 8) | ((n & 0x55555555) << 8)
        return n 