class Solution(object):
    def isMatch(self, s, p):
        """
        :type s: str
        :type p: str
        :rtype: bool
        """
        n = len(s)
        m = len(p)
        dp = [[False for j in xrange(m + 1)] for i in xrange(n + 1)]
        dp[0][0] = True

        for j in xrange(2, m + 1):
            if p[j - 1] == "*":
                dp[0][j] = dp[0][j - 2]

        def canMatch(i, j):
            return s[i - 1] == p[j - 1] or p[j - 1] == "."

        for i in xrange(1, n + 1):
            for j in xrange(1, m + 1):
                if canMatch(i, j):
                    dp[i][j] = dp[i - 1][j - 1]
                elif p[j - 1] == "*":
                    if canMatch(i, j - 1):
                        dp[i][j] = dp[i - 1][j - 1] or dp[i - 1][j] or dp[i][j - 2]
                    else:
                        dp[i][j] = dp[i][j - 2]
        print dp
        return dp[n][m]

if __name__ == '__main__':
    test = Solution()
    print(test.isMatch("aaa", ".*"))