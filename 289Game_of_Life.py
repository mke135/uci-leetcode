class Solution(object):
    def gameOfLife(self, board):
        """
        :type board: List[List[int]]
        :rtype: void Do not return anything, modify board in-place instead.
        """
        #00, 01, 10, 11 the status of board, the first digit is the next status
        #the second digit is the current status.
        n = len(board)
        def liveNeighbors(i, j):
            lives = 0
            for p in xrange(max(0, i-1), min(i+2, n)):
                for q in xrange(max(0, j-1), min(j+2, len(board[0]))):
                    lives += board[p][q] % 2
            return lives - board[i][j] % 2
        for i in xrange(n):
            for j in xrange(len(board[0])):
                lives = liveNeighbors(i, j)
                if board[i][j] == 1 and lives >= 2 and lives <= 3:
                    board[i][j] = 3 #11
                elif board[i][j] == 0 and lives == 3:
                    board[i][j] = 2 #10
        #update status
        for i in xrange(n):
            for j in xrange(len(board[0])):
                board[i][j] = 0 if board[i][j] in [0, 1] else 1

if __name__ == '__main__':
    test = Solution()
    test.gameOfLife([
    [0,1,0],
    [0,0,1],
    [1,1,1],
    [0,0,0]
    ])