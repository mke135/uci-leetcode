#coding=utf-8
from collections import defaultdict
from collections import deque
class Solution(object):
    def calcEquation(self, equations, values, queries):
        """
        :type equations: List[List[str]]
        :type values: List[float]
        :type queries: List[List[str]]
        :rtype: List[float]
        """
        relations = defaultdict(dict)
        for i, x in enumerate(equations):
            a, b = x
            relations[a][b] = values[i]
            relations[b][a] = float(1)/values[i]
        ret = []
        #bfs search
        def bfsSearch(a, b):
            visit = set([a])
            queue = deque([[a, 1]])
            while len(queue):
                q, val = queue.popleft()
                for x in relations[q]:
                    if x not in visit:
                        visit.add(x)
                        score = relations[q][x] * float(val)
                        queue.append([x, score])
                        relations[a][x] = score
                        if b in relations[a]:
                            ret.append(score)
                            return 
            ret.append(-1.0)

        for a, b in queries:
            if a == b and a in relations:
                ret.append(1.0)
            elif a in relations and b in relations:
                if a in relations[b]:
                    ret.append(float(1)/relations[b][a])
                elif b in relations[a]:
                    ret.append(relations[a][b])
                else:
                    bfsSearch(a, b)
            else:
                ret.append(-1.0)
        return ret

if __name__ == '__main__':
    test = Solution()
    print test.calcEquation([["x1","x2"],["x2","x3"],["x3","x4"],["x4","x5"]],
[3.0,4.0,5.0,6.0],
[["x1","x5"],["x5","x2"],["x2","x4"],["x2","x2"],["x2","x9"],["x9","x9"]])