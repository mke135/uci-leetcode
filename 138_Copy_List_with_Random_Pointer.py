# Definition for singly-linked list with a random pointer.
class RandomListNode(object):
    def __init__(self, x):
        self.label = x
        self.next = None
        self.random = None

class Solution(object):
    def copyRandomList(self, head):
        """
        :type head: RandomListNode
        :rtype: RandomListNode
        """
        ret = RandomListNode(-1) #fake head
        cur = head
        cur_new = ret
        old2new = {}
        while cur:
            new_node = RandomListNode(cur.label)
            old2new[cur] = new_node
            cur_new.next = new_node
            cur = cur.next
            cur_new = new_node
        cur = head
        cur_new = ret.next
        while cur:
            old_random = cur.random
            if old_random:
                cur_new.random = old2new[old_random]
            cur = cur.next
            cur_new = cur_new.next
        return ret.next