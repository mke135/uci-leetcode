class Solution(object):
    def merge(self, nums1, m, nums2, n):
        """
        :type nums1: List[int]
        :type m: int
        :type nums2: List[int]
        :type n: int
        :rtype: void Do not return anything, modify nums1 in-place instead.
        """
        #start from the last of the nums1 and nums2, save the bigger number into the end of nums1
        i = m - 1
        j = n - 1
        index = m + n - 1
        while j >= 0 and i >= 0:
            if nums1[i] >= nums2[j]:
                nums1[index] = nums1[i]
                i -= 1
            else:
                nums1[index] = nums2[j]
                j -= 1
            index -= 1
        if j >= 0:
            nums1[:index + 1] = nums2[:j + 1]