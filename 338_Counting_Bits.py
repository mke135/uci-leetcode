class Solution(object):
    def countBits(self, num):
        """
        :type num: int
        :rtype: List[int]
        """
        ret = [0] * (num + 1)
        for i in xrange(1, num + 1):
            ret[i] = ret[i // 2] + i % 2
        return ret