class Solution(object):
    def diffWaysToCompute(self, input_s):
        """
        :type input: str
        :rtype: List[int]
        """
        ret = []#possible results
        for i, x in enumerate(input_s):
            if x in ['+', '-', '*']:
                left = self.diffWaysToCompute(input_s[:i])
                right = self.diffWaysToCompute(input_s[i + 1:])
                for p in left:
                    for q in right:
                        if x == '+':
                            ret.append(p + q)
                        if x == '-':
                            ret.append(p - q)
                        if x == '*':
                            ret.append(p * q)
                
        return ret if len(ret) > 0 else [int(input_s)]

if __name__ == '__main__':
    test = Solution()
    print test.diffWaysToCompute("2*3-4*5")
            