class Solution(object):
    def addStrings(self, num1, num2):
        """
        :type num1: str
        :type num2: str
        :rtype: str
        """
        carry = 0
        n = len(num1)
        m = len(num2)
        ret = ['0'] * (max(m, n) + 1)
        for i in xrange(1, len(ret) + 1):
            x = num1[-i] if i <= n else 0
            y = num2[-i] if i <= m else 0
            ret[-i] = str((int(x) + int(y) + carry) % 10)
            carry = (int(x) + int(y) + carry) // 10
            
        return ''.join(ret[1:]) if ret[0] == '0' else ''.join(ret)