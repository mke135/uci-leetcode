class Solution(object):
    def countSubstrings(self, s):
        """
        :type s: str
        :rtype: int
        """
        self.ret = 0
        n = len(s)
        
        def findPalin(left, right):
            while left >= 0 and right < n and s[left] == s[right]:
                self.ret += 1
                left -= 1
                right += 1

        for i in xrange(n):
            findPalin(i, i)#odd length of palindromic subarray
            findPalin(i, i+1)
        
        return self.ret
