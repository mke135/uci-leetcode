from collections import defaultdict
class Solution(object):
    def palindromePairs(self, words):
        """
        :type words: List[str]
        :rtype: List[List[int]]
        """
        word2index = defaultdict(int)
        for i, w in enumerate(words):
            word2index[w] = i
        
        def isPalindrome(seq):
            l, r = 0, len(seq) - 1
            while l < r:
                if seq[l] != seq[r]:
                    return False
                l += 1
                r -= 1
            return True
        ret = []
        for i, w in enumerate(words):
            for j in xrange(len(w)):
                prefix = w[:j]
                suffix = w[j:]
                rev_p, rev_s = prefix[::-1], suffix[::-1]
                
                if rev_p in word2index and isPalindrome(suffix) and i != word2index[rev_p]:
                    if rev_p == "":
                        ret.append([word2index[rev_p], i])
                    ret.append([i, word2index[rev_p]])
                if rev_s in word2index and isPalindrome(prefix) and word2index[rev_s] != i:
                    ret.append([word2index[rev_s], i])
        return ret