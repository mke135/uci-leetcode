class Solution(object):
    def intersection(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: List[int]
        """
        ret = []
        number_set = set()
        for x in nums1:
            number_set.add(x)
        for x in nums2:
            if x in number_set:
                ret.append(x)
                number_set.remove(x)
        return ret